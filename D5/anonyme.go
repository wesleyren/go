package main

import (
	"fmt"
	"time"
)

func main() {
	var user struct {
		id       int
		name     string
		tel      string
		addr     string
		birthday time.Time
	}
	// user = struct {
	// 	id       int
	// 	name     string
	// 	tel      string
	// 	addr     string
	// 	birthday time.Time
	// }{10, "kk", "xxx", "xxxxx", time.Now()} //结构体类型{}
	// fmt.Printf("%T, %#v\n", user, user)
	// user = struct {
	// 	id       int
	// 	name     string
	// 	tel      string
	// 	addr     string
	// 	birthday time.Time
	// }{id:101, name:"kk1"} //结构体类型{}
	// fmt.Printf("%T, %#v\n", user, user)
	var u2=struct {
		id int
		name string
	}{11,"aa"}
	fmt.Printf("%T, %#v\n", u2, u2)
}
