package funcs

import (
	"fmt"
	"io/ioutil"
	"log"
)

/*
1.读取文件
2.全转为小写
3.统计小写字母
map
    for
    关系运算
    string
*/
func Dream() {
	var res=make(map[byte]int)
	var file="./files/dream.txt"
	//读取文件
	data,err:=ioutil.ReadFile(file)
	if err!=nil {
		log.Fatal(err)
	}
	//fmt.Println(data)
	for i,asc:=range data {
		if asc < 91 && asc> 64 {
			data[i]=asc+32
		} else if asc < 97 || asc > 122 {
			data[i]=0
		}
	}
	for _,letter:=range data {
		res[letter]++
	}
	//fmt.Println(res)
	for i,v:=range res {
		fmt.Printf("字母:%c,次数:%v\n",i,v)
	}
}