package funcs

//找出最大的数字
func GetMax(nums []int) (int,int) {
	var max,index int
	for i:=0;i<len(nums)-1;i++ {
		//两两比较
		if nums[i] > max {
			max,index=nums[i],i
		} else {
			continue
		}
	}
	return max,index
}
// func main() {
// 	nums:=[]int{108, 107, 105, 109, 103, 102}
// 	max,index:=getMax(nums)
// 	fmt.Printf("最大数:%v,索引:%v\n",max,index)
// }