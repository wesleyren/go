package main

//homework "gitee.com/wesleyren/go/tree/master/D1/homework/funcs"

import (
	"fmt"

	homework "gitee.com/wesleyren/go/tree/master/D1/homework/funcs"
)

func main() {
	fmt.Println("猜数字")
	homework.NumGuess()
	fmt.Println("乘法表")
	homework.Table()
}