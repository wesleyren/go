/*
 * @,@Author: ,: your name
 * @,@Date: ,: 2021-02-24 15:29:53
 * @,@LastEditTime: ,: 2021-02-24 15:52:01
 * @,@LastEditors: ,: Please set LastEditors
 * @,@Description: ,: In User Settings Edit
 * @,@FilePath: ,: \go\D2\dream.go
 */
package main

import (
	"fmt"
	"io/ioutil"
	"log"
)

func main() {
	var file = "./files/dream.txt"
	var context = make(map[byte]int)
	//以字节方式读取文件
	data, err := ioutil.ReadFile(file)
	//fmt.Println(data)
	if err != nil {
		log.Fatal(err)
	}
	//统计大小写字母并将大写字母转换成小写字母
	//ASCII a-z 97-112 A-Z 65-90
	for i, asc := range data {
		if asc >= 65 && asc <= 90 {
			data[i] = asc + 32
		} else if asc < 97 || asc > 112 {
			data[i] = 0
		}
	}
	//fmt.Println(data)
	//统计字母数
	for _, letter := range data {
		context[letter]++
	}
	//fmt.Println(context)
	for i, v := range context {
		if i != 0 {
			fmt.Printf("letter:%c,count:%v\n", i, v)
		}
	}
}
