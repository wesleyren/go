package routers

import (
	"gitee.com/wesleyren/go/D3/users/v4/controllers"
	"gitee.com/wesleyren/go/D3/users/v4/manager"
)

func init() {
	manager.Register("add",controllers.AddUser)
	manager.Register("modify",controllers.ModifyUser)
	manager.Register("del",controllers.DelUser)
	manager.Register("query",controllers.QueryUser)
	manager.Register("qmc",controllers.QueryMachine)
}