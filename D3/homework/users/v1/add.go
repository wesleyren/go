package main

import (
	"fmt"
	"strconv"
)

//定义存储用户信息的切片(map类型)
//用户包括多个属性，使用map存储
//key=>string, value=>string
//id 编号
//name 姓名
//addr 地址
//tel 电话

//声明一个切片变量
var users=[]map[string]string{}

//获取用户ID的函数getID
func getId() string{
	id:=0
	//获取最大值
	for _,user:=range users{
		if uid,err:=strconv.Atoi(user["id"]);err==nil {
			if uid > id {
				uid=id
			}
		}
	}
	//最新ID
	return strconv.Itoa(id+1)
}
//输入函数input
func input(prompt string) string {
	var text string
	fmt.Print(prompt)
	fmt.Scan(&text)
	return text
}

//增加用户函数addUser
func addUser() {
	//声明一个映射变量
	user:=map[string]string {
		//使用短声明
		"id":getId(),
		"name":input("请输入用户名："),
		"addr":input("请输入地址："),
		"tel":input("请输入电话："),
	}
	users=append(users, user)
}

func main() {
	addUser()
	fmt.Printf("%#v\n",users)
}