package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	file,err:=os.Open("test.txt")
	if err!=nil {
		return
	}
	defer file.Close()
	reader:=bufio.NewReader(file)
	content:=make([]byte,3)
	n,err:=reader.Read(content)
	fmt.Println(n,err,content[:n])
	fmt.Println(reader.ReadByte())
	//reader.Write
}