package main

import (
	"fmt"
	"log"

	"gitee.com/wesleyren/go/tree/master/D6/homework/userMgt/cmd/funcs"
	"gitee.com/wesleyren/go/tree/master/D6/homework/userMgt/define"
	"gitee.com/wesleyren/go/tree/master/D6/homework/userMgt/utils"
)

func main() {
	serv()
}

func serv() {
	//operater
	var op string

	//add some users and map cmd to funcs
	funcs.Init(&define.UserList)

	//login
	if !funcs.Login() {
		return
	}

	//login prompt
	funcs.ShowHelp() 
	fmt.Print(">")
	// main loop for manager users
	for {
		op=utils.Read()
		//exec the corresponding func of the cmd
		err:=funcs.ExecFunc(op)
		fmt.Print(">")
		if err!=nil {
			log.Fatal(err)
		}		
	}
}