package main

import (
	"fmt"
	"strings"
)

//定义存储用户信息的切片(map类型)
//用户包括多个属性，使用map存储
//key=>string, value=>string
//id 编号
//name 姓名
//addr 地址
//tel 电话

//声明一个切片变量
var users=[]map[string]string{}

//初始化函数init，造9条数据
func init() {
	for i:=0;i<10;i++ {
		users=append(users,map[string]string {
			"id":fmt.Sprintf("%d",i),
			"name":fmt.Sprintf("name_%d",i),
			"addr":fmt.Sprintf("addr_%d",i),
			"tel":fmt.Sprintf("tel_%d",i),
		})
	}
}

//输入函数input
func input(prompt string) string {
	var text string
	fmt.Print(prompt)
	fmt.Scan(&text)
	return text
}

//按ID查找用户
func findUserById(id string) map[string]string {
	for _,user:=range users {
		if user["id"]==id {
			return user
		}
	}
	return nil
}

//按ID修改用户
func modifyUserById(user map[string]string,id string) {
	//用tuser,否则被覆盖
	for i,tuser:=range users {
		if tuser["id"]==id {
			users[i] = user
			break
		}
	}
}

//修改用户函数
func modifyUser() {
	id:=input("请输入要修改用户的ID：")
	user:=findUserById(id)
	if user==nil {
		fmt.Println("用户不存在")
	} else {
		fmt.Println("你要修改的用户信息如下：")
		fmt.Println(user)
		confirm:=input("确认修改吗？Y/N ")
		if strings.ToLower(confirm)=="y" || strings.ToLower(confirm)=="yes" {
			user:=map[string]string {
				"id":id,
				"name":input("请输入用户名："),
				"addr":input("请输入地址："),
				"tel":input("请输入电话："),
			}
			modifyUserById(user,id)
		}
	}
}

func main() {
	fmt.Printf("%#v\n",users)
	modifyUser()	
	fmt.Println(users)
}