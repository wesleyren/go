package controllers

import (
	"fmt"
	"strings"

	"gitee.com/wesleyren/go/D3/users/v4/models"
	"gitee.com/wesleyren/go/D3/users/v4/utils"
)

//增加用户函数addUser
func AddUser() {
	//声明一个映射变量
	user:=map[string]string {
		"name":utils.Input("请输入用户名："),
		"addr":utils.Input("请输入地址："),
		"tel":utils.Input("请输入电话："),
	}
	//users=append(users, user)
	//对数据进行操作
	models.AddUser(user)
	//用户响应
	fmt.Println("用户添加成功")
}

//修改用户函数
func ModifyUser() {
	id:=utils.Input("请输入要修改用户的ID：")
	user:=models.FindUserById(id)
	if user==nil {
		fmt.Println("用户不存在")
	} else {
		fmt.Println("你要修改的用户信息如下：")
		fmt.Println(user)
		confirm:=utils.Input("确认修改吗？Y/N ")
		if strings.ToLower(confirm)=="y" || strings.ToLower(confirm)=="yes" {
			user:=map[string]string {
				"id":id,
				"name":utils.Input("请输入用户名："),
				"addr":utils.Input("请输入地址："),
				"tel":utils.Input("请输入电话："),
			}
			//modifyUserById(user,id)
			//对数据进行操作
			models.ModifyUserById(user,id)
		}
	}
}
//删除用户函数delUser
func DelUser() {
	id := utils.Input("请输入需要删除的用户ID: ")
	user := models.FindUserById(id)
	if user == nil {
		fmt.Println("用户信息不存在")
	} else {
		fmt.Println("你将要删除的用户信息如下: ")
		fmt.Println(user)
		confirm := utils.Input("确定删除吗?(Y/n)")
		if strings.ToLower(confirm) == "y" || strings.ToLower(confirm) == "yes" {
			models.DelUserById(id)
		}
	}
}
func QueryUser() {
	//输入
	q:=utils.Input("请输入查询信息：")
	fmt.Println("查询结果")
	//调用models对数据过滤
	users:=models.QueryUser(q)
	for _,user:=range users {
		printUser(user)
	}
}
//打印用户信息printUser
func printUser(user map[string]string){
	fmt.Println(user)
	//fmt.Println(user["id"])
}