package main

import (
	"fmt"
	"time"
)

type Addr struct {
	province string
	street   string
	no       string
}

type Tel struct {
	prefix string
	number string
}

type User struct {
	id int
	name string
	addr *Addr
	tel *Tel
	birthday time.Time
}

func main() {
	var user User=User{addr:&Addr{province:"sx"}}
	
	user.addr.province="bj"
	fmt.Println(user.addr.province)

	u2:=user
	u2.addr.province="xx"
	fmt.Println(user.addr.province)
}