/*
 * @,@Author: ,: your name
 * @,@Date: ,: 2021-03-09 08:29:14
 * @,@LastEditTime: ,: 2021-03-09 13:07:42
 * @,@LastEditors: ,: Please set LastEditors
 * @,@Description: ,: In User Settings Edit
 * @,@FilePath: ,: \go\D3\home\funcs\showUser.go
 */
package funcs

import (
	"fmt"
	"os"
	"strconv"

	define "gitee.com/wesleyren/go/tree/master/D5/homework/cmd/define"
	"github.com/olekukonko/tablewriter"
)
func ShowUserList(userlist *[]define.User) {
	t := tablewriter.NewWriter(os.Stdout)
	t.SetAutoFormatHeaders(false)
	t.SetAutoWrapText(false)
	t.SetReflowDuringAutoWrap(false)

	t.SetHeader([]string{"ID", "Name", "Phone", "Addr","Born","Passwd"})
	for _, user := range *userlist {
		id:=strconv.FormatUint(uint64(user.Id),10)
		p:=fmt.Sprintf("%x",user.Passwd)
		t.Append([]string{id, user.Name, user.Phone, user.Addr,user.Born.Format("2020.01.02"),p})
	}
	t.Render()
}

func ShowCurrentUserList() {
	userlist:=&define.UserList
	ShowUserList(userlist)
}

func ShowUser(id int64) {
	userlist:=&define.UserList
	t:=tablewriter.NewWriter(os.Stdout)
	for _,user:=range *userlist {
		if user.Id==id {
			s:=strconv.FormatInt(id,10)
			p:=fmt.Sprintf("%x",user.Passwd)
			t.Append([]string{s, user.Name, user.Phone, user.Addr,user.Born.Format("2020.01.02"),p})
		}
	}
	t.Render()
}
