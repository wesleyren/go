/*
 * @,@Author: ,: your name
 * @,@Date: ,: 2021-03-09 08:29:19
 * @,@LastEditTime: ,: 2021-03-09 12:56:08
 * @,@LastEditors: ,: Please set LastEditors
 * @,@Description: ,: In User Settings Edit
 * @,@FilePath: ,: \go\D3\home\funcs\init.go
 */
package funcs

import (
	define "gitee.com/wesleyren/go/tree/meastr/D3/home/define"
)
func NewUser(n,p,a string) define.User {
	return define.User {
		Name:n,
		Phone:p,
		Addr:a,
	}
}

//初始化数据
func Init() {
	user0:=NewUser("n0","00000000000","a0")
	user1:=NewUser("n1","11111111111","a1")
	user2:=NewUser("n2","22222222222","a2")
	AddUser(&define.UserList,user0)
	AddUser(&define.UserList,user1)
	AddUser(&define.UserList,user2)
	ShowUserList()
}