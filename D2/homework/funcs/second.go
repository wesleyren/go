package funcs

//将第二大的移动到切片的最后第二位
func Second(nums []int)[]int {
	//找出最大，第二大数字的索引
	var maxIdx,subIdx int
	for i,v:=range nums {
		if nums[maxIdx] < v {
			subIdx=maxIdx
			maxIdx=i
		} else {
			if nums[subIdx] < v {
				subIdx=i
			}
		}
	}
	//var newNums=make([]int,len(nums)) 不能指定长度
	var newNums=[]int{}
	for i,v:=range nums {
		if i!=maxIdx && i!=subIdx {
			newNums=append(newNums,v)
		}
	}
	//fmt.Println(newNums)
	newNums=append(newNums,nums[subIdx],nums[maxIdx])
	return newNums
}

// func main() {
// 	nums:=[]int{108, 107, 105, 109, 103, 102}
// 	newNums:=second(nums)
// 	fmt.Printf("新数组:%v\n",newNums)
// }