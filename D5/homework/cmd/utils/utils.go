/*
 * @,@Author: ,: your name
 * @,@Date: ,: 2021-03-09 08:38:23
 * @,@LastEditTime: ,: 2021-03-09 17:35:46
 * @,@LastEditors: ,: Please set LastEditors
 * @,@Description: ,: In User Settings Edit
 * @,@FilePath: ,: \go\D3\home\utils\utils.go
 */
package utils

import (
	"bufio"
	"crypto/md5"
	"fmt"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"time"

	define "gitee.com/wesleyren/go/tree/master/D5/homework/cmd/define"
)

//日期合法性检查
 func DateCheck(d string) error {
	 _,err:=time.Parse("2021.03.09",d)
	 return err
 }
 
 //产生用户ID
 func GenId() (res int64) {
	 result:=time.Now().Unix()
	 return result
 }
 
 //判断电话是否全是数字
 func JustDigits(s string) bool {
	 var d bool = true
	 for _,c:=range s {
		 if c <'0' ||c>'9'{
			 d=false
			 break
		 }
		 if len(s)!=11 {
			 d=false
			 fmt.Println("长度非11位")
			 break
		 }
	 }
	 return d
 }
 
 //标准输入读取数据
 func Read() string{
	 scanner:=bufio.NewScanner(os.Stdin)
	 scanner.Scan()
	 //逐行读取
	 line:=strings.TrimSpace(scanner.Text())
	 return line
 }
 
 //GetField
 func GetField(f string) string {
	 for _, field := range define.UserField {
		 if field==f {
			 fmt.Printf("请输入%v:",f)
			 input:=Read()
			 return input
		 }
	 }
	 return f
 }

 //产生密码
 func GenPasswd() [16]uint8 {
	 d:=[]byte(Read())
	 return md5.Sum(d)
 }
 
 //GetKeyByValue return the same value's different keys
 func GetKeyByValue(m map[string]string,value string) string {
	 var keys=make([]string,5)
	 var cmds string
	 for k,v:=range m {
		 if v==value {
			 keys=append(keys,k)
		 }
	 }
	 for _,key:=range keys {
		 cmds+=fmt.Sprintf("%s",key)
	 }
	 return cmds
 }
 
 //清空控制台
 //创建map存储数据
 var clear map[string]func()
 func init() {
	 clear=make(map[string]func()) //初始化
	 clear["windows"]=func()  {
		 cmd:=exec.Command("cmd","/c","cls")
		 cmd.Stdout=os.Stdout
		 cmd.Run()
	 }
 }
 
 func ClearScreen() {
	 value,ok:=clear[runtime.GOOS]
	 if ok {
		 value()
		 //fmt.Println(ok)
	 } else {
		 panic("操作系统不支持")
	 }
 }
 
 //打印调试信息
 func Message(v string) {
	 fmt.Println(v)
 }
 
 //退出
 func Quit() {
	 os.Exit(define.UserQuit)
 }