package main

import (
	"fmt"
	"time"
)

type User struct {
	id 	int
	name string
	addr string
	tel	string
	birthday time.Time
}

func main() {
	var user User
	fmt.Printf("%T,%#v\n",user,user)
	user=User{10,"aa","xian","111",time.Now()}
	fmt.Printf("%#v\n",user)

	user=User{name:"bb",id:11}
	fmt.Printf("%#v\n",user)
	fmt.Println(user.id)
	user.id=100
	user.name="ccc"

	fmt.Printf("%#v\n",user)
}