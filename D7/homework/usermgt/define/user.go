package define

import (
	"time"
)

const (
	AdminName string = "admin"
	AdminID int64 = 0
	UserQuit int = 1
)

type User struct {
	ID int64 `json:"id`
	Name string `json:"name`
	Address streing `json:"address"`
	Cell string `json:"cell"`
	Born time.Time `json:"born"`
	Passwd string `json:"passwd"`
}

var UserList []User

var UserField []string = []string{"ID","Name","Address","Cell","Born","Passwd"}