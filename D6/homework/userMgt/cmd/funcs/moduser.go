package funcs

import (
	"fmt"
	"strconv"
	"strings"

	"gitee.com/wesleyren/go/tree/master/D6/homework/userMgt/define"
	"gitee.com/wesleyren/go/tree/master/D6/homework/userMgt/utils"
)
func ModifyUser() {
	var input,name string
	fmt.Print("Who you want to modify\n>")
	input=utils.Read()
	if s,err:=strconv.Atoi(strings.TrimSpace(input));err==nil {
		id:=int64(s)
		user,err:=IDFindUser(&define.UserList,id)
		if err != nil {
			fmt.Println("No such user.", user)
		} else {
			ShowUser(id)
			fmt.Printf("Find user: %v\nAre you sure to modify %v?(y/n)\n> ", user.Name, user.Name)
			input = utils.Read()
			if strings.ToLower(input) == "y" {
				user, err := IDModUser(&define.UserList, id)
				if err != nil {
					fmt.Println(err)
				} else {
					fmt.Printf("Modified user: %v\n", user.Name)
					ShowUser(id)
				}
			} else if strings.ToLower(input) == "n" {
				fmt.Println("Nothing changed.")
			}
		}
	} else {
		name = strings.ToLower(strings.TrimSpace(input))
		//fmt.Printf("nameType: %T  nameValue: %v\n", name, name)
		user, err := NameFindUser(&define.UserList, name)
		if err != nil {
			fmt.Println("No such user.", user)
		} else {
			ShowUser(user.ID)
			fmt.Printf("Find user: %v\nAre you sure to modify %v?(y/n)\n> ", user.Name, user.Name)
			input = utils.Read()
			if strings.ToLower(input) == "y" {
				user, err := NameModUser(&define.UserList, name)
				fmt.Printf("user: %#v\nerr: %v", user, err)
				if err != nil {
					fmt.Println(err)
				} else {
					fmt.Printf("Modified user: %v\n", user.Name)
					ShowUser(user.ID)
				}
			} else if strings.ToLower(input) == "n" {
				fmt.Println("Nothing changed.")
			}
		}
	}
}