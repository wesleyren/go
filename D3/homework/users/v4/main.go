package main

import (
	"fmt"

	"gitee.com/wesleyren/go/D3/users/v4/manager"

	//没有被使用,采用下划线导入
	_ "gitee.com/wesleyren/go/D3/users/v4/routers"
	"gitee.com/wesleyren/go/D3/users/v4/utils"
)

//声明一个切片变量

var password="e10adc3949ba59abbe56e057f20f883e"

//认证函数
func auth()bool {
	for i := 0; i < 3; i++ {
		if utils.Md5text(utils.Input("请输入密码: ")) == password {
			return true
		} else {
			fmt.Println("输入密码错误")
		}
	}
	return false
}
func main() {
	if !auth() {
		fmt.Println("密码输入错误，程序退出！")
		return
	}
	manager.Run()
}