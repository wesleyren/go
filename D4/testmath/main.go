package main

import (
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"fmt"
)

//sprintf不输出到控制台
func main() {
	txt:="aaabbb"
	md5Value := fmt.Sprintf("%X",md5.Sum([]byte(txt)))
	fmt.Println(md5Value)
	sha1Value := fmt.Sprintf("%X", sha1.Sum([]byte(txt)))
	fmt.Println(sha1Value)
	sha256Value := fmt.Sprintf("%X", sha256.Sum256([]byte(txt)))
	fmt.Println(sha256Value)
	sha512Value := fmt.Sprintf("%X", sha512.Sum512([]byte(txt)))
	fmt.Println(sha512Value)

	md5Hasher:=md5.New()
	md5Hasher.Write([]byte("aa"))
	md5Hasher.Write([]byte("bbb"))
	fmt.Printf("%X",md5Hasher.Sum(nil))	//零值
}