/*
 * @,@Author: ,: your name
 * @,@Date: ,: 2021-03-09 08:29:04
 * @,@LastEditTime: ,: 2021-03-09 15:29:14
 * @,@LastEditors: ,: Please set LastEditors
 * @,@Description: ,: In User Settings Edit
 * @,@FilePath: ,: \go\D3\home\funcs\queryUser.go
 */
package funcs

import (
	"fmt"
	"strings"

	define "gitee.com/wesleyren/go/tree/master/D5/homework/cmd/define"
	utils "gitee.com/wesleyren/go/tree/master/D5/homework/cmd/utils"
)

func QueryUser() {
	userlist:=&define.UserList
	var input string
	var gotUsers []define.User
	fmt.Println("请输入要查询的信息")
	input=utils.Read()
	for _,user:=range *userlist {
		if ContainsInput(user,input) {
			gotUsers=append(gotUsers,user)
		}		
	}
	ShowUserList(&gotUsers)
}

func ContainsInput(u define.User,input string) bool {
	return strings.Contains(strings.ToLower(u.Name),input) ||
	strings.Contains(strings.ToLower(u.Addr),input) ||
	strings.Contains(u.Phone,input) ||
	strings.Contains(u.Born.Format("2021.01.02"),input)
}