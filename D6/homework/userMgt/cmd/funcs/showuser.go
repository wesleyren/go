package funcs

import (
<<<<<<< HEAD
=======
	"gitee.com/wesleyren/go/tree/master/D6/homework/userMgt/define"
<<<<<<< HEAD
>>>>>>> fab14c7366fe5da015364ccd20aad0774240f140
=======
>>>>>>> fab14c7366fe5da015364ccd20aad0774240f140
	"os"
	"strconv"

	"gitee.com/wesleyren/go/tree/master/D6/homework/userMgt/define"
	"github.com/olekukonko/tablewriter"
)

//ShowUser show a user based on ID
func ShowUser(id int64) {
	t:=tablewriter.NewWriter(os.Stdout)
	for _,user:=range define.UserList {
		if user.ID==id {
			s:=strconv.FormatInt(id,10)
			t.Append([]string{s,user.Name,user.Cell,user.Address,user.Born.Format("2006.01.02"), user.Passwd})
		}
	}
	t.Render()
}

func ShowUserList(ul *[]define.User) {
	t:=tablewriter.NewWriter(os.Stdout)
	t.SetAutoFormatHeaders(false)
	t.SetAutoWrapText(false)
	t.SetReflowDuringAutoWrap(false)
	t.SetHeader([]string{"ID", "Name", "Cell", "Address", "Born", "Passwd"})
	for _,user := range *ul {
		id:=strconv.FormatUint(uint64(user.ID),10)
		t.Append([]string{id,user.Name,user.Cell,user.Address,user.Born.Format("2006.01.02"), user.Passwd})
	}
	t.Render()
}

//ShowCurrentUserList make takes no arg
func ShowCurrentUserList() {
	ul:=&define.UserList
	ShowUserList(ul)
}