/*
 * @,@Author: ,: your name
 * @,@Date: ,: 2021-03-09 08:38:23
 * @,@LastEditTime: ,: 2021-03-09 17:35:46
 * @,@LastEditors: ,: Please set LastEditors
 * @,@Description: ,: In User Settings Edit
 * @,@FilePath: ,: \go\D3\home\utils\utils.go
 */
package utils

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"runtime"
	"strconv"
	"strings"
	"time"

	define "gitee.com/wesleyren/go/tree/meastr/D4/homework/define"
	"github.com/olekukonko/tablewriter"
)

//日期合法性检查
func DateCheck(d string) error {
	_,err:=time.Parse("2021.03.09",d)
	return err
}

//产生用户ID
func GenId() (res int64) {
	result:=time.Now().Unix()
	return result
}

//判断电话是否全是数字及11位
func JustDigits(s string) bool {
	var d bool = true
	for _,c:=range s {
		if c <'0' ||c>'9'{
			d=false
			break
		}
		if len(s)!=11 {
			d=false
			fmt.Println("长度非11位")
			break
		}
	}
	return d
}

//标准输入读取数据
func Read() string{
	scanner:=bufio.NewScanner(os.Stdin)
	scanner.Scan()
	//逐行读取
	line:=strings.TrimSpace(scanner.Text())
	return line
}

<<<<<<< HEAD
//GetField
func GetField(f string) string {
	for _,field:=range define.UserField() {
		if field==f {
			fmt.Printf("请输入%v:",f)
			input:=Read()
			return input
=======
//通过ID来显示用户信息
func ShowUser(Id int64) {
	t:=tablewriter.NewWriter(os.Stdin)
	for _,userMap:=range define.UserList {
		if i,ok:=userMap[Id];ok {
			s:=strconv.FormatInt(Id,10)
			t.Append([]string{s,i.Name,i.Phone,i.Addr})
		}
		t.Render()
	}
}
//基于ID查找用户
func QueryUserById(UserList []map[int64]define.User,Id int64) define.User {
	var user define.User
	for _,userMap:=range UserList {
		if _,ok:=userMap[Id];ok {
			user=userMap[Id]
>>>>>>> e9332e2fc6f59f6af372a6643db1997d44fd8bd3
		}
	}
	return f
}
<<<<<<< HEAD

//产生密码
func GenPasswd() [16]unit8 {
	d:=[]byte(Read())
	return md5.Sum(d)
=======
//基于用户名查找用户
func QueryUserByName(Name string) map[int64]define.User {
	var user map[int64]define.User
	var id int64
	for _,userMap:=range define.UserList {
		for i,v:=range userMap {
			for v.Name==Name {
				user=map[int64]define.User{i:define.User{v.Name,v.Addr,v.Phone}}
				id=i
				break
			}
		}	
	}
	if string(id)=="" {
		fmt.Println("用户不存在")
	}
	return user
>>>>>>> e9332e2fc6f59f6af372a6643db1997d44fd8bd3
}

//GetKeyByValue return the same value's different keys
func GetKeyByValue(m map[string]string,value string) string {
	var keys=make([]string,5)
	var cmds string
	for k,v:=range m {
		if v==value {
			keys=append(keys,k)
		}
	}
	for _,key:=range keys {
		cmds+=fmt.Sprintf("%s",key)
	}
	return cmds
}
// //通过ID来显示用户信息
// func ShowUser(Id int64) {
// 	for _,userMap:=range define.UserList {
// 		if i,ok:=userMap[Id];ok {
// 			//fmt.Println(Id,i,ok)
// 			//fmt.Println(i.Name,i.Addr,i.Phone)
// 			//格式化输出
// 			w:=tabwriter.NewWriter(os.Stdout,0,0,1,' ',0|tabwriter.Debug)
// 			s:=strconv.FormatInt(Id,10)
// 			fmt.Fprintln(w,"|"+s+"\t"+i.Name+"\t"+i.Phone+"\t"+i.Addr+"|")
// 			w.Flush()
// 			break
// 		}
// 	}
// }
// //基于ID查找用户
// func QueryUserById(UserList []map[int64]define.User,Id int64) define.User {
// 	var user define.User
// 	for _,userMap:=range UserList {
// 		if _,ok:=userMap[Id];ok {
// 			user=userMap[Id]
// 		}
// 	}
// 	return user
// }
// //基于用户名查找用户
// func QueryUserByName(Name string) map[int64]define.User {
// 	var user map[int64]define.User
// 	var id int64
// 	for _,userMap:=range define.UserList {
// 		for i,v:=range userMap {
// 			for v.Name==Name {
// 				user=map[int64]define.User{i:define.User{v.Name,v.Addr,v.Phone}}
// 				id=i
// 				//fmt.Println(user)
// 				break
// 			}
// 		}	
// 		// fmt.Println(user)
// 	}
// 	if string(id)=="" {
// 		fmt.Println("用户不存在")
// 	}
// 	//fmt.Println(user)
// 	return user
// }

// //基于ID删除用户
// func DelUserById(user *[]map[int64]define.User,id int64) {
// 	for i,u:=range *user {
// 		for k,_:=range u {
// 			if int64(k)==id {
// 				*user=append(define.UserList[:i],define.UserList[i+1:]...)
// 			}
// 		}
// 	}
// }
// //基于name删除用户
// func DelUserByName(user *[]map[int64]define.User,name string) {
// 	for i,u:=range *user {
// 		for _,v:=range u {
// 			if v.Name==name {
// 				*user=append(define.UserList[:i],define.UserList[i+1:]...)
// 			}
// 		}
// 	}
// }

// //基于ID修改用户
// func ModifyUserById(user *[]map[int64]define.User,id int64) define.User {
// 	//用户输入的新数据
// 	var iname,iaddr,iphone string
// 	//新用户数据
// 	var newUser define.User
// 	for _,u:=range *user {
// 		for k,v:=range u {
// 			if int64(k)==id {
// 				fmt.Printf("输入新用户名%v:",v.Name)
// 				iname=Read()
// 				//判断用户是否重名
// 				// for JustName(iname) == false {
// 				// 	//fmt.Println("请输入数字")
// 				// 	iname=Read()
// 				// 	if JustName(iphone)==true {
// 				// 		break
// 				// 	}
// 				// }
// 				newUser.Name=iname
// 				if iname=="" {
// 					newUser.Name=v.Name
// 				}
// 				fmt.Printf("输入新地址%v:",v.Addr)
// 				iaddr=Read()
// 				newUser.Addr=iaddr
// 				if iaddr=="" {
// 					newUser.Addr=v.Addr
// 				}
// 				fmt.Printf("输入新联系方式%v:",v.Phone)
// 				iphone=Read()
// 				for JustDigits(iphone) == false {
// 					//fmt.Println("请输入数字")
// 					iphone=Read()
// 					if JustDigits(iphone)==true {
// 						break
// 					}
// 				}
// 				newUser.Phone=iphone
// 				if iphone=="" {
// 					newUser.Phone=v.Phone
// 				}
// 				u[k]=newUser
// 				fmt.Printf("修改用户：%v:%v\n",k,newUser)
// 			}
// 		}
// 	}
// 	return newUser
// }

// //基于用户名修改用户
// func ModifyUserByName(user *[]map[int64]define.User,name string) {
// 	var iname,iaddr,iphone string
// 	var newUser define.User
// 	for _,u:=range *user {
// 		for k,v:=range u {
// 			if v.Name==name {
// 				fmt.Printf("输入新用户名%v:",v.Name)
// 				iname=Read()
// 				newUser.Name=iname
// 				if iname=="" {
// 					newUser.Name=v.Name
// 				}
// 				fmt.Printf("输入新地址%v:",v.Addr)
// 				iaddr=Read()
// 				newUser.Addr=iaddr
// 				if iaddr=="" {
// 					newUser.Addr=v.Addr
// 				}
// 				fmt.Printf("输入新联系方式%v:",v.Phone)
// 				iphone=Read()
// 				for JustDigits(iphone) == false {
// 					//fmt.Println("请输入数字")
// 					iphone=Read()
// 					if JustDigits(iphone)==true {
// 						break
// 					}
// 				}
// 				newUser.Phone=iphone
// 				if iphone=="" {
// 					newUser.Phone=v.Phone
// 				}
// 				u[k]=newUser
// 				fmt.Printf("修改用户：%v:%v\n",k,newUser)
// 			}
// 		}
// 	}
// }

//清空控制台
//创建map存储数据
var clear map[string]func()
func init() {
	clear=make(map[string]func()) //初始化
	clear["windows"]=func()  {
		cmd:=exec.Command("cmd","/c","cls")
		cmd.Stdout=os.Stdout
		cmd.Run()
	}
}

func ClearScreen() {
	value,ok:=clear[runtime.GOOS]
	if ok {
		value()
		//fmt.Println(ok)
	} else {
		panic("操作系统不支持")
	}
}

//打印调试信息
func Message(v string) {
	fmt.Println(v)
}

//退出
func Quit() {
	os.Exit(define.UserQuit)
}