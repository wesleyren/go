package db

import (
	"encoding/csv"
	"log"
	"os"
	"path/filepath"
	"strconv"

	"gitee.com/wesleyren/go/tree/master/D6/homework/userMgt/define"
)

//Save will flush the define.UserList's contents into a file periodically
func Save(ul *[]define.User) {
	csvFile,err:=os.Create(filepath.Join(dbDir,dbName))
	if err!=nil {
		log.Fatal(err)
	}
	writer:=csv.NewWriter(csvFile)
	for _,user:=range *ul {
		writer.Write([]string{strconv.FormatInt(user.ID,10),user.Name,user.Cell,user.Address,user.Born.Format("2006.01.02"),user.Passwd})
	}
	writer.Flush()
}

//SaveUsers wrap the save func
func SaveUsers() {
	Save(&define.UserList)
}