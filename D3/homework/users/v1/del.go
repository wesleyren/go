package main

import (
	"fmt"
	"strings"
)

//定义存储用户信息的切片(map类型)
//用户包括多个属性，使用map存储
//key=>string, value=>string
//id 编号
//name 姓名
//addr 地址
//tel 电话

//声明一个切片变量
var users=[]map[string]string{}

//初始化函数init，造9条数据
func init() {
	for i:=0;i<10;i++ {
		users=append(users,map[string]string {
			"id":fmt.Sprintf("%d",i),
			"name":fmt.Sprintf("name_%d",i),
			"addr":fmt.Sprintf("addr_%d",i),
			"tel":fmt.Sprintf("tel_%d",i),
		})
	}
}

//过滤,是否包含filter
func filter(user map[string]string,q string) bool {
	return strings.Contains(user["name"],q) ||
		strings.Contains(user["addr"],q) || 
		strings.Contains(user["tel"], q)
}

//打印用户信息printUser
func printUser(user map[string]string){
	fmt.Println(user)
	//fmt.Println(user["id"])
}

//输入函数input
func input(prompt string) string {
	var text string
	fmt.Print(prompt)
	fmt.Scan(&text)
	return text
}

//按ID查找用户
func findUserById(id string) map[string]string {
	for _,user:=range users {
		if user["id"]==id {
			return user
		}
	}
	return nil
}

//按ID删除用户
func delUserById(id string) {
	//方法1：不删除-》新的切片-》users
	//定义新切片,用make提前申请内存地址空间
	// tempUsers:=make([]map[string]string,0,len(users)-1)
	// for _,user:=range users {
	// 	if user["id"] !=id {
	// 		tempUsers=append(tempUsers, user)
	// 	}
	// }
	// users=tempUsers
	//方法2: 
	//a. index->append([:n],[n+1:])
	for i,user:=range users {
		if user["id"]==id {
			users = append(users[:i], users[i+1:]...)
		}
	}
	//b. copy (dst, src) src->dst
	// for i,user:=range users {
	// 	if user["id"]==id {
	// 		dst:=users[i:len(users)]
	// 		src:=users[i+1:len(users)]
	// 		copy(dst,src)
	// 		users=users[0:len(users)-1]
	// 	}
	// }
}
//删除用户函数delUser
func delUser() {
	id:=input("请输入要删除用户的ID：")
	user:=findUserById(id)
	if user==nil {
		fmt.Println("用户不存在")
	} else {
		fmt.Println("你要删除的用户信息如下：")
		fmt.Println(user)
		confirm:=input("确认删除吗？Y/N ")
		if strings.ToLower(confirm)=="y" || strings.ToLower(confirm)=="yes" {
			delUserById(id)
		}
	}
}

func main() {
	fmt.Printf("%#v\n",users)
	delUser()	
	fmt.Println(users)
}