package main

import (
	"fmt"
	"time"
)

type Addr struct {
	province string
	street   string
	no       string
}

type Tel struct {
	prefix string
	number string
}

type User struct {
	id int
	name string
	Addr
	Tel 
	birthday time.Time
	province string
}

func main() {
	var user User=User{Addr:Addr{province:"sx"}}
	
	user.Addr.province="bj"
	fmt.Println(user.Addr.province)

	u2:=user
	u2.Addr.province="xx"
	fmt.Println(u2.Addr.province)
}