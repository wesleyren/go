package funcs

import (
	"fmt"
	"os"

	"github.com/olekukonko/tablewriter"
)
func showHelp(funcList map[string]string) {
	t := tablewriter.NewWriter(os.Stdout)
	t.SetAutoFormatHeaders(false)
	t.SetAutoWrapText(false)
	t.SetReflowDuringAutoWrap(false)
	t.SetHeader([]string{"CMD","Function"})
	for cmd,f := range funcList {
		t.Append([]string{cmd,f})
	}
	t.Render()
}

func ShowHelp() {
	showHelp(FuncList)
}

func DefaultTip() {
	fmt.Print("\n|type \"h\" show help list|\n> ")
}