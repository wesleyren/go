/*
 * @,@Author: ,: your name
 * @,@Date: ,: 2021-03-09 08:29:14
 * @,@LastEditTime: ,: 2021-03-09 13:07:42
 * @,@LastEditors: ,: Please set LastEditors
 * @,@Description: ,: In User Settings Edit
 * @,@FilePath: ,: \go\D3\home\funcs\showUser.go
 */
package funcs

import (
	"fmt"
	"os"
	"strconv"
	"text/tabwriter"
	define "gitee.com/wesleyren/go/tree/meastr/D3/home/define"
)
 func ShowUserList() {
	 fmt.Println("用户列表")
	 fmt.Println("|.....Id.....|.....Name.....|.....Phone.....|.....Address.....|")
	 for _,user:=range define.UserList {
		 for i,v:=range user {
			 w:=tabwriter.NewWriter(os.Stdout,0,0,1,' ',0|tabwriter.Debug)
			 s:=strconv.FormatInt(i,10)
			 fmt.Fprintln(w, "|"+s+"\t"+v.Name+"\t"+v.Phone+"\t"+v.Addr+" |")
			 w.Flush()
		 }
	 }
	 fmt.Println("")
 }