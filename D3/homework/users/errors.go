package main

import (
	"errors"
	"fmt"
)

func div(left,right int) (int, error) {
	if right==0 {
		return 0,fmt.Errorf("zero")
	}
	return left/right,nil
}

func div2(left,right int) (int,error) {
	if right==0 {
		return 0,errors.New("zero")
	}
	return left/right,nil
}

func main() {

	if v,err:=div(1,0);err==nil {
		fmt.Println(v)
	} else {
		fmt.Println(err)
	}
	fmt.Println(div(2,1))
	fmt.Println(div2(1,0))
	fmt.Println(div2(2,1))
}