package main

import (
	"flag"
	"fmt"
)

func main() {
	var port int
	var pwd string
	var help bool

	flag.IntVar(&port,"P",22,"port")
	flag.BoolVar(&help,"h",false,"help")
	flag.StringVar(&pwd,"p","","pwd")

	flag.Parse()
	if help {
		//fmt.Println("help")
		//flag.PrintDefaults()
		flag.Usage()
		return
	}
	fmt.Println(port,pwd)
	fmt.Println(flag.Args())
}