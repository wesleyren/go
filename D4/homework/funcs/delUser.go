/*
 * @,@Author: ,: your name
 * @,@Date: ,: 2021-03-09 08:28:48
 * @,@LastEditTime: ,: 2021-03-09 16:07:10
 * @,@LastEditors: ,: Please set LastEditors
 * @,@Description: ,: In User Settings Edit
 * @,@FilePath: ,: \go\D3\home\funcs\delUser.go
 */
package funcs

import (
	"fmt"
	"strconv"
	"strings"

	define "gitee.com/wesleyren/go/tree/meastr/D4/homework/define"
	utils "gitee.com/wesleyren/go/tree/meastr/D4/homework/utils"
)

func DelUser() {
	 var name,input string
	 fmt.Println("输入你想删除的用户的ID/name")
	 input=utils.Read()
	 if i,err:=strconv.Atoi(strings.TrimSpace(input));err==nil {
		 id:=int64(i)
		 user:=utils.QueryUserById(define.UserList,id)
		 if user.Name=="" {
			 fmt.Println("用户不存在")
		 } else {
			 fmt.Printf("找到%v,确认要删除么%v?(y/n)\n",name,user,name)
			 input=utils.Read()
			 if strings.ToLower(input)=="y" {
				 fmt.Println("删除")
				 utils.DelUserById(&define.UserList,id)
			 }else if strings.ToLower(input)=="n" {
				 fmt.Println("未操作")
			 }else {
				 fmt.Println("输入错误")
			 }
		 }
	 } else {
		 name=strings.ToLower(strings.TrimSpace(input))
		// fmt.Printf("nameType: %T  nameValue: %v\n", name, name)
		 user:=utils.QueryUserByName(name)
		 if user==nil {
			 fmt.Println("用户不存在")
		 } else {
			fmt.Printf("找到%v,确认要删除么%v?(y/n)\n",name,user)
			// fmt.Println(name,user)
			// fmt.Println("确认要删除么？(y/n)")
			input=utils.Read()
			if strings.ToLower(input)=="y" {
				fmt.Println("删除")
				utils.DelUserByName(&define.UserList,name)
			}else if strings.ToLower(input)=="n" {
				fmt.Println("未操作")
			}else {
				fmt.Println("输入错误")
			}
		 }
	 }
}