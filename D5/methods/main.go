package main

import (
	"fmt"
	"methods/models"
)
func main() {
	user:=models.NewUser(1,"kk",32)
	//fmt.Println(user)
	//models.AddAge(user)
	//models.AddAge(user)
	// user.AddAge()
	// fmt.Println(user)
	//fmt.Println(models.GetName(user))
	//fmt.Println(user.GetName())
	//fmt.Println(user.AddAge())
	puser:=&user
	//(*puser).AddAge()
	// fmt.Println(*puser)
	// fmt.Println((*puser).GetName())
	puser.AddAge()
	fmt.Println(*puser)
	fmt.Println(puser.GetName())
	puser.AddAge()
	fmt.Println(*puser)
}