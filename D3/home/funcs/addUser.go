/*
 * @,@Author: ,: your name
 * @,@Date: ,: 2021-03-09 08:28:41
 * @,@LastEditTime: ,: 2021-03-09 13:06:48
 * @,@LastEditors: ,: Please set LastEditors
 * @,@Description: ,: In User Settings Edit
 * @,@FilePath: ,: \go\D3\home\funcs\addUser.go
 */
package funcs

import (
	"fmt"
	define "gitee.com/wesleyren/go/tree/meastr/D3/home/define"
	utils "gitee.com/wesleyren/go/tree/meastr/D3/home/utils"
)
func AddUser(userList *[]map[int64]define.User,user define.User) {
	//生成ID
	idc:=utils.GenId()
	userc:=map[int64]define.User{idc:user}
	define.UserList=append(*userList,userc)
	//fmt.Printf("用户%v添加\n",userc[idc].Name)
}

//添加用户
func AddCurrentUser() {
	var name,addr,phone string
	fmt.Println("请输入用户名：")
	name=utils.Read()
	fmt.Println("请输入地址")
	addr=utils.Read()
	fmt.Println("请输入联系方式")
    phone=utils.Read()
	for utils.JustDigits(phone)==false {
		fmt.Println("请输入数字")
		fmt.Scan(&phone)
		if utils.JustDigits(phone)==true {
			break
		}
	} 
	AddUser(&define.UserList,NewUser(name,phone,addr))
}