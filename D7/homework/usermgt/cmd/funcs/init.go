package funcs

import (
	"crypto/md5"
	"fmt"
	"strings"
	"time"

	"gitee.com/wesleyren/go/tree/master/D7/homework/usermgt/define"
	"gitee.com/wesleyren/go/tree/master/D7/homework/usermgt/utils"
)
func NewUser(id int64,name,cell,address,born,passwd string) define.User{
	return define.User {
		ID: id,
		Name: name,
		Cell: cell,
		Address: address,
		Born: func() time.Time {
			t, _ := time.Parse("2006.01.02", born)
			return t
		}(),
		Passwd: fmt.Sprintf("%x",md5.Sum([]byte(passwd))),
	}
}

func AddSomeOne() {
	fmt.Print("There's a user admin in db, will you add yourself to db?(y/n)\n> ")
	input := utils.Read()
	if strings.ToLower(input) == "y" {
		AddUser()
	} else if strings.ToLower(input) == "n" {
		fmt.Println("Nothing changes.")
	}
}

func Init(ul *[]define.User) {
	user0 := NewUser(0, "admin", "18811992299", "HaidianDistrict,BeijingXinParkRestaurants,BeixiaguanSubdistrict,HaidianDistrict,China",
		time.Now().Format("2006.01.02"), "qwert")
	(*ul) = append((*ul), user0)
	AddFunc()
	db.ReadUsers()
	fmt.Print("There's a user admin in db, will you add yourself to db?(y/n)\n> ")
	input := utils.Read()
	if strings.ToLower(input) == "y" {
		AddUser()
	} else if strings.ToLower(input) == "n" {
		fmt.Println("Nothing changes.")
	}
}