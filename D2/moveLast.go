/*
 * @,@Author: ,: your name
 * @,@Date: ,: 2021-02-24 16:04:18
 * @,@LastEditTime: ,: 2021-02-24 16:20:07
 * @,@LastEditors: ,: Please set LastEditors
 * @,@Description: ,: In User Settings Edit
 * @,@FilePath: ,: \go\D2\moveLast.go
 */
package main

import "fmt"

//返回切片
func moveLast(numsArray []int) []int {
	var newArrays = make([]int, len(numsArray))
	maxNumber, idMax := maxNum(numsArray)
	for i := 0; i < len(numsArray); i++ {
		if i > idMax {
			newArrays[i-1] = numsArray[i]
		} else {
			newArrays[i] = numsArray[i]
		}
	newArrays[len(newArrays)-1]=maxNumber
	return newArrays
}

func maxNum(numsArray []int) (int, int) {
	var maxNum, id int
	for i := 0; i < len(numsArray); i++ {
		if numsArray[i] > maxNum {
			maxNum, id = numsArray[i], i
		} else {
			continue
		}
	}
	return maxNum, id
}

func main() {
	var numsArray = []int{108, 107, 105, 109, 103, 102}
	movedNums := maxNum(numsArray)
	fmt.Printf("数组：%v\n", movedNums)
	
}
