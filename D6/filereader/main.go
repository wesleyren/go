package main

import (
	"fmt"
	"io"
	"os"
)

func main() {
	path:="test.txt"
	file,err:=os.Open(path)
	fmt.Println(file,err)
	if err!=nil {
		return
	}
	defer file.Close()
	content:=make([]byte,3) //切片
	for {
		n,err:=file.Read(content)
		if err!=nil {
			if err!=io.EOF {
				fmt.Println(err)
			}
			break
		}
		fmt.Println(string(content[:n]))
	}
}