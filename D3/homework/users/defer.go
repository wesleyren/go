package main

import "fmt"

func main() {
	fmt.Println("start")
	defer func() {
		fmt.Println("defer 1")
	}()
	defer func() {
		fmt.Println("defer 2")
	}()
	fmt.Println("end")
}