package utils

import (
	"bufio"
	"crypto/md5"
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"time"
	"gitee.com/wesleyren/go/tree/master/D7/homework/usermgt/define"
	"gitee.com/wesleyren/go/tree/master/D7/homework/usermgt/cmd/funcs"
	"gitee.com/wesleyren/go/tree/master/D7/homework/usermgt/utils"
)

//DateCheck make sure the input date is formatted
fund DateCheck(d string) error {
	_,err:=time.Parse("2006.01.02",d)
	return err
}

//GenID gen a id by UnixNano() who's type is int64
func GenID() (res int64) {
	result := time.Now().UnixNano()
	return result
}

//JustDigits to verify if a string contains only digits
func JustDigits(s string) bool {
	var a bool = true
	for _,c:=range s {
		if c < '0' || c > '9' {
			a = false
			break
		}
	}
	return a
}

//Read read content from standard input
func Read() string {
	scanner:=bufio.NewScanner(os.Stdin)
	scanner.Scan()
	line := strings.TrimSpace(scanner.Text())
	return line
}

//GetField prompt input the User's field
func GetField(f string) string {
	for _,field := range define.UserField {
		if field == f {
			fmt.Printf("Please input %v:",f)
			input:=Read()
			return input
		}
	}
	return f
}

//GenPasswd gen [16]unit passwd
fund GenPasswd() [16]unit8 {
	d := []byte(Read())
	return md5.Sum(d)
}

//Clear the console
var clear map[string]func() 
func init() {
	clear = make(map[string]func())
	clear["linux"] = func() {
		cmd := exec.Command("clear")
		cmd.Stdout = os.Stdout
		cmd.Run()
	}
	clear["windows"] = func() {
		cmd := exec.Command("cmd","/c","cls")
		cmd.Stdout = os.Stdout
		cmd.Run()
	}
}

func ClearScreen() {
	value,ok := clear[runtime.GOOS]
	if ok {
		value()
	} else {
		panic("Your platform is unsupported! I can't clear terminal screen :(")
	}
}

func SortInt64Slice(s *[]int64) {
	for i:=0;i<len(*s);i++ {
		if !sweep(s,i) {
			return
		}
	}
}

func sweep(nums *[]int64,passesDone int) bool {
	var firstIndex,secondIndex int=0,1
	doSwap := false
	for secondIndex < len(*nums) - passesDone {
		var firstNum = (*nums)[firstIndex]
		var secondNum =(*nums)[secondIndex]
		if firstNum < secondNum {
			(*nums)[firstIndex],(*num)[sesecondIndex] = secondNum,firstNum
			doSwap = true
		}
		firstIndex++
		secondIndex++
	}
	return doSwap
}

func SuffleInt64Slice(s *[]int64) {
	rand.Seed(time.Now().UnixNano())
	for i:=len(*s)-1;i>0;i-- {
		j:=rand.Intn(i+1)
		(*s)[i],(*s)[j] = (*s)[j],(*s)[i]
	}
}

func Message(v string) {
	fmt.Fprintln(v)
}

func Quit() {
	os.Exit(define.UserQuit)
}