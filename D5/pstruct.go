package main

import (
	"fmt"
	"time"
)

type User struct {
	id 	int
	name string
	addr string
	tel	string
	birthday time.Time
}

func NewUser(id int,name,addr,tel string,birthday time.Time) *User {
	return User{id,name,addr,tel,birthday}
}
func main() {
	var user =&User{id:10,name:"aa"}
	fmt.Printf("%T,%#v\n",user,user)
	user = new(User)
	fmt.Printf("%T,%#v\n",user,user)
	u2:=new(User)
	fmt.Printf("%T,%#v\n",u2,u2)
	u2.name="kk"
	fmt.Println(u2.name)
	u3:=NewUser(1,"bb","sh","aa",time.Now())
	fmt.Printf("%T,%#v\n",u3,u3)
	// user=User{10,"aa","xian","111",time.Now()}
	// fmt.Printf("%#v\n",user)

	// user=User{name:"bb",id:11}
	// fmt.Printf("%#v\n",user)
	// fmt.Println(user.id)
	// user.id=100
	// user.name="ccc"

	// fmt.Printf("%#v\n",user)
}