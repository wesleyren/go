package main

import "fmt"

type Counter int
type Counter2 Counter

type User map[string]string
type Callback func() error
func main() {
	var user User=make(User)
	fmt.Printf("%T,%v\n",user,user)
	user["id"]="1"
	fmt.Println(user)
	
	callbacks:=map[string]Callback{}
	callbacks["add"]=func() error {
		fmt.Println("add")
		return nil
	}
	callbacks["add"]()

	var c22 Counter2
	fmt.Printf("%T,%v\n",c22,c22)
}