package funcs

import (
	"crypto/md5"
	"fmt"

	"gitee.com/wesleyren/go/tree/master/D6/homework/userMgt/define"
	"gitee.com/wesleyren/go/tree/master/D6/homework/userMgt/utils"
)

//Login implement login func
func Login() bool {
	var logCount int8=0
	var logged bool = true
	for {
		fmt.Print("Input the username[admin]:\n>")
		input:=utils.Read()
		user,err:=NameFindUser(&define.UserList,input)
		if err!=nil {
			fmt.Println("no such user")
			logCount++
			if logCount==3 {
				logged=false
				break
			}
		} else {
			fmt.Print("Input passwd[123456]:\n>")
			input:=utils.Read()
			inputPasswd:=fmt.Sprintf("%x",md5.Sum([]byte(input)))
			if user.Passwd==inputPasswd {
				fmt.Printf("User %s logged in.\n",user.Name)
				return logged
			} else {
				fmt.Println("wrong passwd")
			}
			logCount++
			if logCount== 3 {
				logged=false
				break
			}
			continue
		}
	}
	return logged
}