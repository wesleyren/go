package main

import (
	"os"

	"github.com/olekukonko/tablewriter"
)

func main() {
	t:=tablewriter.NewWriter(os.Stdout)
	t.SetAutoFormatHeaders(false)
	t.SetAutoWrapText(false)
	t.SetReflowDuringAutoWrap(false)

	t.SetHeader([]string {"ID","name","link"})
	t.Append([]string{"1","aa","11111"})
	t.Render()
}