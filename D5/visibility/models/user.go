package models

type PublicStruct struct {
	PublicAttrPu string
	privateAttrPu string
}

type privateStruct struct {
	PublicAttrPr string
	privateAttrPr string
}

func NewPrivateStruct() *privateStruct {
	return &privateStruct{}
}

// type CombindStruct struct {
// 	PublicAttr PublicStruct
// 	PrivateAttr privateStruct
// }

type CombindStruct struct {
	PublicStruct
	privateStruct
}