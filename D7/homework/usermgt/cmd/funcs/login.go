package funcs

import (
	"crypto/md5"
	"fmt"

	"gitee.com/wesleyren/go/tree/master/D7/homework/usermgt/define"
	"gitee.com/wesleyren/go/tree/master/D7/homework/usermgt/utils"
)

func Login()bool {
	var logCount int8=0
	var logged bool = true
	for {
		fmt.Print("Input the UserName[admin]: \n> ")
		input := utils.Read()
		user,err := NameFindUser(&define.UserList,input)
		if err!=nil {
			fmt.Println("No such user.")
			logCount++
			if logCount == 3 {
				logged = false
				break
			}
		} else {
			fmt.Print("Input the PassWord[qwert]: \n> ")
			input := utils.Read()
			inputPasswd := fmt.Sprintf("%x",md5.Sum([]byte(input)))
			if user.Passwd == inputPasswd {
				fmt.Printf("User %s logged in.\n", user.Name)
				return logged
			} else {
				fmt.Println("Wrong password...")
			}
			logCount++
			if logCount == 3 {
				logged = false
				break
			}
			continue

		}
	}
	return logged
}