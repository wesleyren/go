/*
 * @,@Author: ,: your name
 * @,@Date: ,: 2021-03-09 08:28:41
 * @,@LastEditTime: ,: 2021-03-09 17:36:36
 * @,@LastEditors: ,: Please set LastEditors
 * @,@Description: ,: In User Settings Edit
 * @,@FilePath: ,: \go\D3\home\funcs\addUser.go
 */
package funcs

import (
	define "gitee.com/wesleyren/go/tree/master/D5/homework/cmd/define"
	utils "gitee.com/wesleyren/go/tree/master/D5/homework/cmd/utils"
)

//输入次数
var count int=3
//添加用户
func AddUser() {
	//生成ID
	userlist:=&define.UserList
	var uc define.User
	var name string
	id:=GetMaxId(userlist) +1
	for i:=0;i<count;i++ {
		name=utils.GetField("Name")
		user,err:=NameFindUser(userlist,name)
		//判断用户是否存在
		if err==nil {
			utils.Message("用户已存在")
			utils.Message(user.Name)
		} else if name=="" {
			utils.Message("必须输入用户名")
		} else {
			break
		}
		if i==count-1 {
			utils.Message("机会用完")
			return
		}
	}
	phone := utils.GetField("Phone")
	for i:=0;i<count;i++ {
		if !utils.JustDigits(phone) {
			utils.Message("请输入有效的电话号码")
			phone=utils.GetField("Phone") 			
		} else {
			break
		}
		if i==count-1 {
			utils.Message("机会用完")
			return
		}
	}
	addr:=utils.GetField("Addr")
	born := utils.GetField("Born")
	for i:=0;i<count;i++ {
		if err:=utils.DateCheck(born);err!=nil {
			utils.Message("请输入有效的日期")
			born=utils.GetField("Born") 
		} else {
			break
		}
		if i == count-1 {
			utils.Message("机会用完")
			return
		}
	}
	passwd:=utils.GetField("Passwd")
	uc=NewUser(id,name,addr,phone,born,passwd)
	define.UserList=append(*userlist,uc)
}