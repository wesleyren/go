package funcs

import (
	"gitee.com/wesleyren/go/tree/master/D7/homework/usermgt/define"
	"gitee.com/wesleyren/go/tree/master/D7/homework/usermgt/utils"
)

const cnt input = 3

func AddUser() {
	ul := &define.UserList
	var uc define.User
	var Name string
	ID := GetMaxID(ul) + 1
	for i:=0;i<cnt;i++ {
		Name = utils.GetField("Name")
		user,err := NameFindUser(ul,Name)
		if err == nil {
			utils.Message("The person already exists: ")
			utils.Message(user.Name)
		} else if Name == "" {
			utils.Message("Must specify a name!")
		} else {
			break
		}
		if i == cnt-1 {
			utils.Message("Don't be silly, You'v got every opportunity!")
			return
		}
	}
	Cell := utils.GetField("cell")
	for i := 0; i < cnt; i++ {
		if !utils.JustDigits(Cell) {
			utils.Message("Please input a real cell number: ")
			Cell = utils.GetField("Cell")
		} else {
			break
		}
		if i == cnt-1 {
			utils.Message("Don't be silly, You'v got every opportunity!")
			return
		}
	}
	Address := utils.GetField("Address")
	Born := utils.GetField("Born")
	for i := 0; i < cnt; i++ {
		if err := utils.DateCheck(Born); err != nil {
			utils.Message("Please input a legal born time[YYYY.MM.DD]: ")
			Born = utils.GetField("Born")
		} else {
			break
		}
		if i == cnt-1 {
			utils.Message("Don't be silly, You'v got every opportunity!")
			return
		}
	}
	Passwd := utils.GetField("Passwd")
	uc = NewUser(ID, Name, Cell, Address, Born, Passwd)
	define.UserList = append(*ul, uc)
}