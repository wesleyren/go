/*
 * @,@Author: ,: your name
 * @,@Date: ,: 2021-02-24 15:52:51
 * @,@LastEditTime: ,: 2021-02-24 16:02:16
 * @,@LastEditors: ,: Please set LastEditors
 * @,@Description: ,: In User Settings Edit
 * @,@FilePath: ,: \go\D2\max.go
 */
package main

import "fmt"

func maxNum(numsArray []int) (int, int) {
	var maxNum, id int
	for i := 0; i < len(numsArray); i++ {
		if numsArray[i] > maxNum {
			maxNum, id = numsArray[i], i
		} else {
			continue
		}
	}
	return maxNum, id
}

func main() {
	var numsArray = []int{108, 107, 105, 109, 103, 102}
	max, id := maxNum(numsArray)
	//max, id := maxNum([]int{108, 107, 105, 109, 103, 102, 110})
	fmt.Printf("最大数是:%v，索引是：%v\n", max, id)
}
