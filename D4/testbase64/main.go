package main

import (
	"encoding/base64"
	"fmt"
)

func main() {
	// 编码 []byte => string
	// 解码 string => []byte
	txt:="111 11aaa 中国"
	fmt.Println(base64.StdEncoding.EncodeToString([]byte(txt)))
	bytes,_:=base64.StdEncoding.DecodeString("MTExMTFhYWE=")
	fmt.Println(string(bytes))
	fmt.Println(base64.RawStdEncoding.EncodeToString([]byte(txt)))
	fmt.Println(base64.URLEncoding.EncodeToString([]byte(txt)))
	// text := "i am kk编码"
	// fmt.Println(base64.StdEncoding.EncodeToString([]byte(text)))
	// bytes, _ := base64.StdEncoding.DecodeString("aSBhbSBraw==")
	// fmt.Println(string(bytes))
	// fmt.Println(base64.RawStdEncoding.EncodeToString([]byte(text)))
	// fmt.Println(base64.URLEncoding.EncodeToString([]byte(text)))
}