package db

import (
	"gitee.com/wesleyren/go/tree/master/D7/homework/usermgt/define"
)
var (
	dbLocation string
	absDir string
	dbAbsFile string
	base = "main.go"
	dbDir = "db"
	dbNameCSV = "userDB.csv"
	dbNameGob = "userDB.gob"
	dbNameJSON = "userDB.json"
	SaveFlag = ""
)

var dbNameList map[string]string = map[string]string {
	"csv": "userDB.csv",
	"gob": "userDB.gob",
	"json": "userDB.json",
}

var tmpUsers []define.User