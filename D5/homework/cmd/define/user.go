package define

import (
	"time"
)

// UserQuit represent quit exit status code
const (
	AdminName string = "admin"
	AdminID   int64  = 0
	UserQuit  int    = 1
)

// User to contain user's info
// UserList to contain all the users
type User struct {
	Id      int64
	Name    string
	Addr string
	Phone    string
	Born    time.Time
	Passwd  [16]byte
}

// UserList contains users
var UserList []User

// UserField slice for GetField func
var UserField []string = []string{"Id", "Name", "Addr", "Phone", "Born", "Passwd"}
