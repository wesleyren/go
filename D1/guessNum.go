/*
 * @,@Author: ,: your name
 * @,@Date: ,: 2021-02-24 14:42:54
 * @,@LastEditTime: ,: 2021-02-24 15:02:27
 * @,@LastEditors: ,: Please set LastEditors
 * @,@Description: ,: In User Settings Edit
 * @,@FilePath: ,: \go\D1\guessNum.go
 */
package main

import (
	"fmt"
	"log"
	"math/rand"
	"time"
)

func main() {
	var guess int
	var thresholde int = 5
	//thresholde := 5
	rand.Seed(time.Now().UnixNano())
	num := rand.Intn(100) + 1
	for i := 1; i <= thresholde; i++ {
		fmt.Println("请输入0-100的数字:")
		_, err := fmt.Scan(&guess)
		if err != nil {
			log.Fatal(err)
		} else if guess > 100 || guess < 1 {
			fmt.Println("输入数据无效！")
			//threshold++
			fmt.Printf("%v次机会剩余\n", thresholde-i)
			continue
		}
		if guess > num {
			fmt.Printf("猜大了，还有%v次机会！\n", thresholde-i)
		} else if guess < num {
			fmt.Printf("猜小了，还有%v次机会！\n", thresholde-i)
		} else if guess == num {
			fmt.Printf("猜对了，还有%v次机会！\n", thresholde-i)
		}
		if i == thresholde {
			fmt.Println("机会用完了")
			fmt.Printf("随机数是：%v\n", num)
		}
	}
}
