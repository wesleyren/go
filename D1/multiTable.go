/*
 * @,@Author: ,: your name
 * @,@Date: ,: 2021-02-24 14:41:46
 * @,@LastEditTime: ,: 2021-02-24 21:12:52
 * @,@LastEditors: ,: Please set LastEditors
 * @,@Description: ,: In User Settings Edit
 * @,@FilePath: ,: \go\D1\multiTable.go
 */
package main

import (
	"fmt"
)

func main() {
	for i := 1; i <= 9; i++ {
		for j := 1; j <= i; j++ {
			fmt.Printf("%d*%d=%d ", i, j, i*j)
		}
		fmt.Println()
	}
}
