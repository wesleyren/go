package main

import (
	"fmt"
)

// func add(a,b int) int {
// 	return a+b
// }

func addAll(a,b int,args ...int) int {
//	fmt.Println(a,b,args)
	//fmt.Printf("%T\n",args)
	print(args...)
	sum:=a+b
	for _,v:=range args {
		sum+=v
	}
	return sum
}
func print(args ...int) {
	for i,v:=range args {
		fmt.Println(i,v)
	}
}
func main() {
	//fmt.Println(add(2,3))
	fmt.Println(addAll(1,2))
	//fmt.Println(addAll(1,2,2))
}