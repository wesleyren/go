/*
 * @,@Author: ,: your name
 * @,@Date: ,: 2021-03-09 08:29:04
 * @,@LastEditTime: ,: 2021-03-09 15:29:14
 * @,@LastEditors: ,: Please set LastEditors
 * @,@Description: ,: In User Settings Edit
 * @,@FilePath: ,: \go\D3\home\funcs\queryUser.go
 */
package funcs

import (
	"fmt"
	"strings"
	utils "gitee.com/wesleyren/go/tree/meastr/D3/home/utils"
	define "gitee.com/wesleyren/go/tree/meastr/D3/home/define"
	
)

func SearchUser(user *[]map[int64]define.User) {
	var input string
	fmt.Println("请输入要查询的信息")
	input=utils.Read()
	for _,userMap:=range *user {
		for i,v:=range userMap {
			if strings.Contains(strings.ToLower(v.Name),input) ||
				strings.Contains(strings.ToLower(v.Addr),input) ||
				strings.Contains(strings.ToLower(v.Phone),input) {
					utils.ShowUser(i)
					}
		}
	}
}