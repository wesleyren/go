package models

import (
	"strconv"
	"strings"
)

var users=[]map[string]string{}

func getId() string{
	id:=0
	//获取最大值
	for _,user:=range users{
		if uid,err:=strconv.Atoi(user["id"]);err==nil {
			if uid > id {
				uid=id
			}
		}
	}
	//最新ID
	return strconv.Itoa(id+1)
}




//按ID查找用户
func FindUserById(id string) map[string]string {
	for _,user:=range users {
		if user["id"]==id {
			return user
		}
	}
	return nil
}

//按ID修改用户
func ModifyUserById(user map[string]string,id string) {
	//用tuser,否则被覆盖
	for i,tuser:=range users {
		if tuser["id"]==id {
			users[i] = user
			break
		}
	}
}


//过滤,是否包含filter
func filter(user map[string]string,q string) bool {
	return strings.Contains(user["name"],q) ||
		strings.Contains(user["addr"],q) || 
		strings.Contains(user["tel"], q) ||
		strings.Contains(user["id"],q)
}

func DelUserById(id string) {
	for i,user:=range users {
		if user["id"]==id {
			users = append(users[:i], users[i+1:]...)
		}
	}

}


func AddUser(user map[string]string) {
	user["id"]=getId()
	users=append(users,user)
}

func QueryUser(q string) []map[string]string {
	//新的切片
	rt := make([]map[string]string, 0, len(users))
	for _, user := range users {
		if filter(user, q) {
			rt = append(rt, user)
		}
	}
	return rt
}