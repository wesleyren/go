package main

import "fmt"

func sayHello() {
	fmt.Println("hello")
}

func add(a, b int) int {
	return a + b
}
func op(a,b int) (int,int,int,int) {
	return a + b, a - b, a * b, a / b
}
func op1(a,b int) (sum,sub,mul,div int) {
	sum=a + b
	sub= a - b
	mul= a * b
	div= a / b
	return
}


func main() {
	sayHello()
	add(1, 2)
	c := add(3, 4)
	fmt.Println(c)
	a,b,c,d:=op(4,2)
	fmt.Println(a,b,c,d)
	fmt.Println(op1(3, 2))
}