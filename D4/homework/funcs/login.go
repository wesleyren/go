package funcs

import (
	"crypto/md5"
	"fmt"

	utils "gitee.com/wesleyren/go/tree/meastr/D4/homework/utils"
)

var p string="admin"
var password [16]byte=md5.Sum([]byte(p))

func Login() bool {
	var logCount int = 0
	var logged bool = true
	for {
		fmt.Println("请输入密码")
		input:=utils.Read()
		inputPasswd:=md5.Sum([]byte(input))
		if password==inputPasswd {
			fmt.Println("登录成功")
			return logged
		} else {
			fmt.Println("密码错误")
			logCount++
			if logCount==5 {
				logged = false
				break
			}
			continue
		}
	}
	return logged
}