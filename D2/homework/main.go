package main

import (
	"fmt"

	homework "gitee.com/wesleyren/go/tree/master/D2/homework/funcs"
)

func main() {
	nums:=[]int{108, 107, 105, 109, 103, 102}

	fmt.Println("1.我有一个梦想统计")
	homework.Dream()

	fmt.Println("2.获取最大数")
	//nums:=[]int{108, 107, 105, 109, 103, 102}
	max,index:=homework.GetMax(nums)
	fmt.Printf("3.最大数:%v,索引:%v\n",max,index)
	
	fmt.Println("4.移动最大数到最后1位")
	newNums:=homework.MoveLast(nums)
	fmt.Printf("新数组:%v\n",newNums)
	
	fmt.Println("5.移动第二大数到倒数第2位")
	secondNums:=homework.Second(nums)
	fmt.Printf("6.新数组:%v\n",secondNums)
	
	fmt.Println("7.随机数的位置")
 	randNums:=homework.RandNums()
 	for i,v:=range randNums {
		for x,y:=range nums {
			if v==y {
				fmt.Printf("随机数切片的索引为：%d，数组索引为:%d\n",i,x)
			} else {
				fmt.Println("未匹配")
			}
		}
	}

	fmt.Println("8.插入排序")
 	insertNums:=homework.InsertSort(nums)
 	fmt.Printf("新数组:%v\n",insertNums)
}