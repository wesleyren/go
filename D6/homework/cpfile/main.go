package main

import (
	"bufio"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
)

var destBase, destDir string

var destFile *os.File

//Abs=Dir/Base
func main() {
	//cmd flag define
	flagSrc := flag.String("src", "F:\\go\\mago\\D6\\homework\\cpfile\\a\\b\\b.txt", "specify source file location")
	flagDest := flag.String("dest", "F:\\go\\mago\\D6\\homework\\cpfile\\a\\c", "specify dest file location")
	flag.Usage = func() {
		fmt.Println("Usage: go run main.go --src ./a/b --dest ./a/c")
	}
	//parse flag
	flag.Parse()
	srcAbs,_:=filepath.Abs(*flagSrc)
	destAbs,_:=filepath.Abs(*flagDest)
	srcAbsInfo,err:=os.Stat(srcAbs)
	destAbsInfo,errDestAbs:=os.Stat(destAbs)
	srcBase:=filepath.Base(srcAbs)
	destBase=filepath.Base(destAbs)
	destDir=filepath.Dir(destAbs)
	

	
	if os.IsNotExist(err) {
		log.Fatal(err)
		return
	}else if srcAbsInfo.IsDir() {
		fmt.Printf("the file\"%v\" is dir, please specify a file.\n",srcBase)
		return
	}
	//copy
	joinSrcDirToDestBase(destAbsInfo,errDestAbs,&srcBase,&destAbs)
	fileCopy(&srcAbs,&destAbs,&srcBase,&destDir)
}

func joinSrcDirToDestBase(fi os.FileInfo,err error, srcAbs,destAbs *string) {
	if err!=nil {
		log.Fatal(err)
	} else if fi.IsDir() {
		(*destAbs)=filepath.Join((*destAbs),filepath.Base(*srcAbs))
		destDir=filepath.Dir(*destAbs)
		destBase=filepath.Base(*destAbs)
	}
}

func fileCopy(srcAbs,destAbs,srcBase,destDir *string) {
	var opt string
	srcFile,err:=os.Open(*srcAbs)
	if err!=nil {
		log.Fatal(err)
	}
	//fmt.Println("srcFile:",srcFile)
	defer srcFile.Close()
	srcReader:=bufio.NewReader(srcFile)
	//fmt.Println("srcReader:",srcReader)
	fileInfoList,err:=ioutil.ReadDir(*destDir)
	if err!=nil {
		log.Fatal(err)
	}
	//fmt.Println(destBase,destDir)
	for _,file:=range fileInfoList{
		if file.Name()==*srcBase {
			fmt.Printf("There's also a file named %v in %v\nAre you want to overwrite it?(y/n):",*srcBase,*destDir)
			fmt.Scanln(&opt)
			opt=strings.TrimSpace(strings.ToLower(opt))
			if opt=="y" {
				destFile,err:=os.Create(*destAbs)
				fmt.Printf("destfile type:%T,value:%v:\n",destFile,destFile)
				defer destFile.Close()
				if err!=nil {
					log.Fatal(err)
				}
				srcReader.WriteTo(destFile)
				return
			}else if opt=="n" {
				fmt.Println("Noting changed")
				return
			} else {
				fmt.Println("Nothing changed")
				return
			}
		}
	}
	destFile,err:=os.Create(*destAbs)
	fmt.Printf("destFile type:%T,value:%v\n",destFile,destFile)
	defer destFile.Close()
	if err!=nil {
		log.Fatal(err)
	}
	_,errww:=srcReader.WriteTo(destFile)
	if err!=nil {
		log.Fatal(errww)
	}
}