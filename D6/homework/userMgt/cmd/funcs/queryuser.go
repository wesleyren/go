package funcs

import (
	"fmt"
	"strings"

	"gitee.com/wesleyren/go/tree/master/D6/homework/userMgt/define"
	"gitee.com/wesleyren/go/tree/master/D6/homework/userMgt/utils"
)

//QueryUser for search one or more user use define.User.Name or define.User.Address or define.User.Cell or define.User.Born
func QueryUser() {
	ul:=&define.UserList
	var input string
	var gotUsers []define.User
	fmt.Print("Please input query string: \n> ")
	input = utils.Read()
	for _,user:=range *ul {
		if ContainsInput(user,input) {
			gotUsers=append(gotUsers,user)
		}
	}
	ShowUserList(&gotUsers)
}

//ContainsInput check if a field of a define.User type user contians a string
func ContainsInput(u define.User,input string) bool {
	return strings.Contains(strings.ToLower(u.Name), input) ||
	strings.Contains(strings.ToLower(u.Address), input) ||
	strings.Contains(u.Cell, input) ||
	strings.Contains(u.Born.Format("2006.01.02"), input)
}