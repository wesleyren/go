package funcs

//将最大的数字移动到切片的最后一位,原来的数字都在移动后的切片中都存在, 只是最大的数字再最后一位


func MoveLast(nums []int) []int {
	var newNums=make([]int,len(nums))
	max,index:=GetMax(nums)
	//将最大数排到最后
	for i,v:=range nums {
		if i!=index {
			newNums[i]=v
		} else {
			continue
		}
	}
	newNums[len(newNums)-1]=max
	return newNums
}

// func getMax(nums []int) (int,int) {
// 	var max,index int
// 	for i:=0;i<len(nums)-1;i++ {
// 		//两两比较
// 		if nums[i] > max {
// 			max,index=nums[i],i
// 		} else {
// 			continue
// 		}
// 	}
// 	return max,index
// }
// func main() {
// 	nums:=[]int{108, 107, 105, 109, 103, 102}
// 	newNums:=moveLast(nums)
// 	fmt.Printf("新数组:%v\n",newNums)
// }