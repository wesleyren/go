package main

import (
	"fmt"
	"strings"
)

//定义存储用户信息的切片(map类型)
//用户包括多个属性，使用map存储
//key=>string, value=>string
//id 编号
//name 姓名
//addr 地址
//tel 电话

//声明一个切片变量
var users=[]map[string]string{}

//初始化函数init，造9条数据
func init() {
	for i:=0;i<10;i++ {
		users=append(users,map[string]string {
			"id":fmt.Sprintf("%d",i),
			"name":fmt.Sprintf("name_%d",i),
			"addr":fmt.Sprintf("addr_%d",i),
			"tel":fmt.Sprintf("tel_%d",i),
		})
	}
}

//过滤,是否包含filter
func filter(user map[string]string,q string) bool {
	return strings.Contains(user["name"],q) ||
		strings.Contains(user["addr"],q) || 
		strings.Contains(user["tel"], q)
}

//打印用户信息printUser
func printUser(user map[string]string){
	fmt.Println(user)
	//fmt.Println(user["id"])
}

//输入函数input
func input(prompt string) string {
	var text string
	fmt.Print(prompt)
	fmt.Scan(&text)
	return text
}

//查询用户函数queryUser
func queryUser() {
	query:=input("请输入查询信息：")	
	fmt.Println("查询结果如下:")
	for _,user := range users {		
		if filter(user,query) ==true {			
			printUser(user)		
		} else {
			fmt.Println("无数据。")
			break
		}
	}
}

func main() {
	fmt.Printf("%#v\n",users)
	queryUser()	
}