package main
	"fmt"
)

// 定义Hello
// 无参，无返回值
func sayHello() {
	fmt.Println("hello")
}
func sayHi(name string) {
	fmt.Println("hi,",name)
}
func add(a  int,b int) int {
	return a+b
}
func main() {
	sayHello()
	sayHi("tony")
	name:="aa"
	sayHi(name)
	c:=add(4,5)
	fmt.Println(c)
}