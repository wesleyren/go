package funcs

import (
	"fmt"
	"strings"

	"gitee.com/wesleyren/go/tree/master/D7/homework/usermgt/define"
	"gitee.com/wesleyren/go/tree/master/D7/homework/usermgt/utils"
)
func QueryUser() {
	ul := &define.UserList
	var input string
	var gotUsers []define.User
	fmt.Print("Please input query string: \n> ")
	input = utils.Read()
	for _, user := range *ul {
		if ContainsIput(user, input) {
			gotUsers = append(gotUsers, user)
		}
	}
	ShowUserList(&gotUsers)
}

// ContainsIput check if a field of a define.User type
// user contains a string
func ContainsIput(u define.User, input string) bool {
	return strings.Contains(strings.ToLower(u.Name), input) ||
		strings.Contains(strings.ToLower(u.Address), input) ||
		strings.Contains(u.Cell, input) ||
		strings.Contains(u.Born.Format("2006.01.02"), input)
}
