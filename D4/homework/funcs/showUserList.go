/*
 * @,@Author: ,: your name
 * @,@Date: ,: 2021-03-09 08:29:14
 * @,@LastEditTime: ,: 2021-03-09 13:07:42
 * @,@LastEditors: ,: Please set LastEditors
 * @,@Description: ,: In User Settings Edit
 * @,@FilePath: ,: \go\D3\home\funcs\showUser.go
 */
package funcs

import (
	"os"
	"strconv"

	define "gitee.com/wesleyren/go/tree/meastr/D4/homework/define"
	"github.com/olekukonko/tablewriter"
)
func ShowUserList() {
	t := tablewriter.NewWriter(os.Stdout)
	t.SetAutoFormatHeaders(false)
	t.SetAutoWrapText(false)
	t.SetReflowDuringAutoWrap(false)

	t.SetHeader([]string{"ID", "Name", "Phone", "Location"})
	for _, user := range define.UserList {
		for k, v := range user {
			s := strconv.FormatInt(k, 10)
			t.Append([]string{s, v.Name, v.Phone, v.Addr})
		}
	}
	t.Render()
}
