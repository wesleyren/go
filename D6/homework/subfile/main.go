package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

var fileOrdir string

func main() {
	//cmd flag
	fileOrdir:=flag.String("f","file-or-dir","specify the file or dir")
	if *fileOrdir == "file-or-dir" {
		fmt.Print("Usage: go run main.go -f /path/to/fileOrDir\n")
	}

	flag.Parse()
	//get abs path
	fileAbs,err:=filepath.Abs(*fileOrdir)
	if err!=nil {
		log.Fatal(err)
	}
	fmt.Println(fileAbs)
	//stat the file
	fileInfo,err:=os.Stat(fileAbs)
	fmt.Println(fileInfo)
	if os.IsNotExist(err) {
		log.Fatal(err)
	} else if fileInfo.IsDir() {
		// if a dir,read it and get a []fileinfo
		fileInfoList,err:=ioutil.ReadDir(fileAbs)
		if err!=nil {
			log.Fatal(err)
		}
		for _,file:=range fileInfoList {
			//each file's abs path
			absfile:=filepath.Join(fileAbs,file.Name())
			suffix:=filepath.Ext(absfile)
			fmt.Println(absfile)
			fmt.Println(suffix)
			if suffix==".go" || suffix==".cgo" {
				fmt.Printf("file: %-25v   dir: %v\n", file.Name(), file.IsDir())
			}
		}
	} else {
		fileDir := filepath.Dir(fileAbs)
		fileInfoList, err := ioutil.ReadDir(fileDir)
		if err!=nil {
			log.Fatal(err)
		}
		for _, file := range fileInfoList {
			absfile := filepath.Join(fileDir, file.Name())
			suffix := filepath.Ext(absfile)
			if suffix == ".go" || suffix == ".cgo" {
				fmt.Printf("file: %-25v   dir: %v\n", file.Name(), file.IsDir())
			}

		}
	}
}