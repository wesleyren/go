package funcs

import (
	"fmt"
	"math/rand"
	"time"
)

func NumGuess() {
	//guess 猜测数
	//target 随机数
	//maxNum猜测最多次数
	var maxNum int = 5
	var guess int
	rand.Seed(time.Now().Unix())
	target:=rand.Intn(100)+1

	for i:=1;i<=maxNum;i++ {
		fmt.Print("输入1-100间的数字:")
		_,err:=fmt.Scan(&guess)
		// input=strings.TrimSpace(input)
		// guess,err:=strconv.Atoi(input)
		//判断输入合法性
		if err!=nil {
			fmt.Println("输入有误")
			maxNum += 1
			continue
		} else if guess <1 || guess> 100 {
			fmt.Println("输入1-100间的数字")
			maxNum += 1
			continue
		}
		//猜数
		if guess>target {
			fmt.Printf("大了，还有%v次机会\n",maxNum-i)
		} else if guess<target {
			fmt.Printf("小了，还有%v次机会\n",maxNum-i)
		} else {
			fmt.Printf("太棒了，%v次就猜中了,目标数是%v\n",i,target)
			break
		}

		if i==maxNum {
			fmt.Printf("机会用完了，目标数是%v\n",target)
			break

		}
	}
}
	