package main

import (
	"fmt"
	"os"
	"strconv"
)

func main() {
	args:=os.Args[1:]
	usage:=func() {
		fmt.Printf("add")
	}
	if len(args) < 2 {
		usage()
		return
	}
	total:=0
	//hasErr:=false
	for _,v:=range args {
		if intV,err:=strconv.Atoi(v);err!=nil {
			usage()
			//hasErr=true
			return
		} else {
			total+=intV
		}
	}
	//if !hasErr {
		fmt.Println(total)
	//}
	
}
