package main

import "fmt"

func main() {
END:
	for  {
		var text string
		fmt.Printf("请输入指令：")
		fmt.Scan(&text)
		// switch text {
		// case "add":
		// 	fmt.Println("添加")
		// case "query":
		// 	fmt.Println("查询")
		// case "del":
		// 	fmt.Println("删除")
		// case "modify":
		// 	fmt.Println("修改")
		// case "exit":
		// 	fmt.Println("退出")
		// 	break END
		// default:
		// 	fmt.Println("输入指令错误。")
		// }
		op:=""
		switch text {
		
		case "add":
			op="添加"
		case "query":
			op="查询"
		case "del":
			op="删除"
		case "modify":
			op="修改"
		case "exit":
			op="退出"
			break END
		default:
			op="输入指令错误。"
		}
		fmt.Println(op)
	}
}