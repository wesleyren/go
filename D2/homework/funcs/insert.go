package funcs

func InsertSort(nums []int)[]int {
	for i:=1;i<len(nums);i++ {
		v:=nums[i]
		j:=i-1
		//
		for j>=0 && nums[j]>v {
			nums[j+1] = nums[j]
			j--
		}
		nums[j+1]=v
	}
	return nums
}


// func main() {
// 	nums:=[]int{108, 107, 105, 109, 103, 102}
// 	newNums:=insertSort(nums)
// 	fmt.Printf("新数组:%v\n",newNums)
// }