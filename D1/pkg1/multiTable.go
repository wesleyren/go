/*
 * @,@Author: ,: your name
 * @,@Date: ,: 2021-02-24 13:05:53
 * @,@LastEditTime: ,: 2021-02-24 14:41:58
 * @,@LastEditors: ,: Please set LastEditors
 * @,@Description: ,: In User Settings Edit
 * @,@FilePath: ,: \go\D1\multiTable.go
 */

package pkg

import (
	"fmt"
)

func multiTable() {
	for i := 1; i <= 9; i++ {
		for j := 1; j <= i; j++ {
			fmt.Printf("%d*%d=%d", i, j, i*j)
		}
		fmt.Println()
	}
}
