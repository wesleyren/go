package main

import (
	"fmt"

	define "gitee.com/wesleyren/go/tree/master/D5/homework/cmd/define"
	funcs "gitee.com/wesleyren/go/tree/master/D5/homework/cmd/funcs"
	utils "gitee.com/wesleyren/go/tree/master/D5/homework/cmd/utils"
)

func main() {
	serv()
}

func serv() {
	var op string
	funcs.Init(&define.UserList)
	if !funcs.Login() {
		return
	}
	funcs.ShowHelp()
	fmt.Print(">")
	for {
		op=utils.Read()
		err:=funcs.ExecFunc(op)
		fmt.Print(">")
		if err!=nil {
			fmt.Print(err)
		}
	}
}