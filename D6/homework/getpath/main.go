package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)
func main() {
	fileOrdir:=flag.String("f","file-or-dir","specify the file or dir")
	if *fileOrdir=="file-or-dir" {
		fmt.Print("Usage:go run main.go -f /path/to/fileOrDir\n")
	}
	fmt.Println(&fileOrdir)
	flag.Parse()

	fileAbs,err:=filepath.Abs(*fileOrdir)
	if err!=nil {
		log.Fatal(err)
	}

	fileInfo,err:=os.Stat(fileAbs)
	if os.IsNotExist(err) {
		log.Fatal(err)
	} else if fileInfo.IsDir() {
		fileInfoList,err:=ioutil.ReadDir(fileAbs)
		if err!=nil {
			log.Fatal(err)
		}
		for _,file:=range fileInfoList {
			fmt.Printf("file:%-25v dir:%v\n",file.Name(),file.IsDir())
		}
	} else {
		fileDir:=filepath.Dir(fileAbs)
		fileInfoList,err:=ioutil.ReadDir(fileDir)
		if err!=nil {
			log.Fatal(err)
		}	
		for _,file:=range fileInfoList {
			fmt.Printf("file: %-25v   dir: %v\n", file.Name(), file.IsDir())
		}
	}
}