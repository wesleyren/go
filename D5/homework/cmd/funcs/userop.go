package funcs

import (
	"crypto/md5"
	"errors"
	"fmt"
	"time"

	define "gitee.com/wesleyren/go/tree/master/D5/homework/cmd/define"
	utils "gitee.com/wesleyren/go/tree/master/D5/homework/cmd/utils"
)

// NewUser make a new user contains user's info
func NewUser(id int64, name, phone, address, born, passwd string) define.User {
	return define.User{
		Id:      id,
		Name:    name,
		Phone:    phone,
		Addr: address,
		Born: func() time.Time {
			t, _ := time.Parse("2006.01.02", born)
			return t
		}(),
		Passwd: md5.Sum([]byte(passwd)),
	}
}

// Init add some users to define.UserList
func Init(ul *[]define.User) {
	user0 := NewUser(0, "admin", "18811992299", "HaidianDistrict,BeijingXinParkRestaurants,BeixiaguanSubdistrict,HaidianDistrict,China",
		time.Now().Format("2006.01.02"), "qwert")
	user1 := NewUser(1, "jack ma", "18800009999", "Hangzhou, China", time.Now().Format("2006.01.02"), "hello")
	user3 := NewUser(3, "steve", "18800002222", "Mars", time.Now().Format("2006.01.02"), "hi")
	define.UserList = append(*ul, user0, user1, user3)
	fmt.Printf("user %v added\n", user0.Name)
	fmt.Printf("user %v added\n", user1.Name)
	fmt.Printf("user %v added\n", user3.Name)
	AddFunc()
}

// IDFindUser find user based on ID
func IdFindUser(ul *[]define.User, id int64) (define.User, error) {
	for _, user := range *ul {
		if user.Id == id {
			return user, nil
		}
	}
	err := errors.New("no such user")
	return define.User{}, err
}

// NameFindUser find user based on Name
func NameFindUser(ul *[]define.User, Name string) (define.User, error) {
	for _, user := range *ul {
		if user.Name == Name {
			return user, nil
		}
	}
	err := errors.New("no such user")
	return define.User{}, err
}

// IDDelUser del user based on ID
func IdDelUser(ul *[]define.User, id int64) error {
	for i, user := range *ul {
		if int64(user.Id) == id {
			if id == define.AdminID {
				err := errors.New("you'r not allowed to modify admin, nothing changed")
				return err
			}
			*ul = append(define.UserList[:i], define.UserList[i+1:]...)
		}
	}
	return nil
}

// NameDelUser del user based on Name
func NameDelUser(ul *[]define.User, name string) error {
	for i, user := range *ul {
		if user.Name == name {
			err := errors.New("you'r not allowed to modify admin, nothing changed")
			return err
		}
		if i == len(*ul) {
			*ul = append(define.UserList[:i], define.UserList[i:]...)
			return nil
		}
		*ul = append(define.UserList[:i], define.UserList[i+1:]...)
	}
	return nil
}

// GetMaxID get max id of current define.UserList
func GetMaxId(ul *[]define.User) int64 {
	var MaxID int64 = -1
	for _, user := range *ul {
		var i int64 = user.Id
		if i > MaxID {
			MaxID = i
		}
	}
	return MaxID
}

// IDModUser modify user based on ID
func IdModUser(ul *[]define.User, id int64) (define.User, error) {
	var iname, iaddress, phone, ipasswd string
	newUser := define.User{Id: id}
	for _, user := range *ul {
		if user.Id == id {
			if id == define.AdminID {
				err := errors.New("you'r not allowed to modify admin, nothing changed")
				return newUser, err
			}

			fmt.Printf("Input new Name [%v]: ", user.Name)
			iname = utils.Read()
			newUser.Name = iname
			if iname == "" {
				newUser.Name = user.Name
			}

			fmt.Printf("Input new Address [%v]: ", user.Addr)
			iaddress = utils.Read()
			newUser.Addr = iaddress
			if iaddress == "" {
				newUser.Addr = user.Addr
			}

			fmt.Printf("Input new Phone [%v]: ", user.Phone)
			phone = utils.Read()
			// make sure the phone number contains only pure digits
			for utils.JustDigits(phone) == false {
				fmt.Print("Please input a legal phone number: \n> ")
				phone = utils.Read()
				if utils.JustDigits(phone) == true {
					break
				}
			}
			newUser.Phone = phone
			if phone == "" {
				newUser.Phone = user.Phone
			}

			fmt.Printf("Input new passwd [%v]: ", user.Passwd)
			ipasswd = utils.Read()
			newUser.Passwd = md5.Sum([]byte(ipasswd))
			if ipasswd == "" {
				newUser.Passwd = user.Passwd
			}
		}
	}
	return newUser, nil
}

// NameModUser modify user based on Name
func NameModUser(ul *[]define.User, name string) (define.User, error) {
	var iname, iaddress, iphone, ipasswd string
	newUser := define.User{}
	for _, u := range *ul {
		if u.Name == name {
			if u.Name == define.AdminName {
				err := errors.New("you'r not allowed to modify admin, nothing changed")
				return newUser, err
			}

			newUser.Id = u.Id
			fmt.Printf("Input new Name [%v]: ", u.Name)
			iname = utils.Read()
			newUser.Name = iname
			if iname == "" {
				newUser.Name = u.Name
			}

			fmt.Printf("Input new Address [%v]: ", u.Addr)
			iaddress = utils.Read()
			newUser.Addr = iaddress
			if iaddress == "" {
				newUser.Addr = u.Addr
			}

			fmt.Printf("Input new Phone [%v]: ", u.Phone)
			iphone = utils.Read()
			// make sure the phone number contains only pure digits
			for utils.JustDigits(iphone) == false {
				fmt.Print("Please input a legal phone number: \n> ")
				iphone = utils.Read()
				if utils.JustDigits(iphone) == true {
					break
				}
			}
			newUser.Phone = iphone
			if iphone == "" {
				newUser.Phone = u.Phone
			}

			fmt.Printf("Input new passwd [%v]: ", u.Passwd)
			ipasswd = utils.Read()
			newUser.Passwd = md5.Sum([]byte(ipasswd))
			if ipasswd == "" {
				newUser.Passwd = u.Passwd
			}
			fmt.Printf("Modified user is: %v:%v\n", newUser.Name, newUser)
			return newUser, nil
		}
	}
	return define.User{}, nil
}
