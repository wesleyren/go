/*
 * @,@Author: ,: your name
 * @,@Date: ,: 2021-03-04 08:17:34
 * @,@LastEditTime: ,: 2021-03-09 08:19:32
 * @,@LastEditors: ,: Please set LastEditors
 * @,@Description: ,: In User Settings Edit
 * @,@FilePath: ,: \go\D3\users\type.go
 */
package main

import "fmt"

func add(a, b int) int {
	return a + b
}

func mul(a, b int) int {
	return a * b
}

func main() {
	//函数类型的变量
	var f func(int, int) int =mul
	
	//函数类型的切片
	var fs []func(int, int) int
	
	fs = append(fs, add, mul)

	c := f(2, 3)
	fmt.Println(c)
	for _, f := range fs {
		fmt.Println(f(2, 3))
	}
} 
