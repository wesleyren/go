package funcs

import (
	"gitee.com/wesleyren/go/tree/master/D7/homework/usermgt/define"
	"gitee.com/wesleyren/go/tree/master/D7/homework/usermgt/cmd/funcs"
	"gitee.com/wesleyren/go/tree/master/D7/homework/usermgt/utils"
	"fmt"
	"strconv"
	"strings"
)

func DelUser() {
	var name,input string
	utils.Message("Who you want to del(ID/Name)?\>n ")
	input = utils.GetField("Name")
	if s,err := strconv.Atoi(strings.TrimSpace(input)); err == nil {
		id := int64(s)
		user,_ := IDFindUser(&define.UserList,id)
		if user.Name == "" {
			fmt.Println("No such user.", user)
		} else {
			ShowUser(id)
			fmt.Printf("Find user: %v\nAre you sure to delete %v?(y/n)\n> ", user.Name, user.Name)
			input = utils.Read()
			if strings.ToLower(input) == "y" {
				if err := IDDelUser(&define.UserList, id); err != nil {
					fmt.Println(err)
				} else {
					fmt.Println("deleted...........")
				}
			} else if strings.ToLower(input) == "n" {
				fmt.Println("Nothing changes.")
			}
		}
	} else {
		name = strings.TrimSpace(input)
		//fmt.Printf("nameType: %T  nameValue: %v\n", name, name)
		user, _ := NameFindUser(&define.UserList, name)
		if (user == define.User{}) {
			fmt.Println("No such user.", user)
		} else {
			ShowUser(user.ID)
			fmt.Printf("Find user: %v\nAre you sure to delete %v?(y/n)\n> ", user.Name, user.Name)
			input = utils.Read()
			if strings.ToLower(input) == "y" {
				if err := NameDelUser(&define.UserList, name); err != nil {
					fmt.Println(err)
				} else {
					fmt.Println("deleted...........")
				}
			} else if strings.ToLower(input) == "n" {
				fmt.Println("Nothing changes.")
			}
		}
	}
}