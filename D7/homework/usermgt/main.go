package main

import (
	"fmt"

	"gitee.com/wesleyren/go/tree/master/D7/homework/usermgt/cmd/funcs"
	"gitee.com/wesleyren/go/tree/master/D7/homework/usermgt/define"
	"gitee.com/wesleyren/go/tree/master/D7/homework/usermgt/utils"
)

func main() {
	serv()
}

func serv() {
	var opt string

	// add some users and map cmd to funcs
	funcs.Init(&define.UserList)

	// login
	if !funcs.Login() {
		return
	}

	// login prompt
	funcs.ShowHelp()
	fmt.Print("> ")
	// main loop for manager users
	for {
		opt = utils.Read()
		// exec the corresponding func of the cmd
		err := funcs.ExecFunc(opt)
		fmt.Print("> ")
		if err != nil {
			fmt.Print(err)
		}
	}
}
