package funcs

import (
	"errors"

	utils "gitee.com/wesleyren/go/tree/meastr/D4/homework/utils"
)
var cmd=[]string{"add","del","mod","query","show","help","cls"}
var funcList=[]func(){AddCurrentUser,DelUser,ModifyUser,ShowUserList,ShowHelp,utils.ClearScreen}
var CmdToFunc = make(map[string]func())

func FuncMap() {
//?
	if len(cmd)==len(funcList) {
		for i,v:=range cmd {
			CmdToFunc[v]=funcList[i]
		}
	}
}

func ExecFunc(input string)error {
	for cmd,_:=range CmdToFunc {
		if cmd==input {
			CmdToFunc[cmd]()
		} else {
			return errors.New("输入命令有误")
		}
	}
	return nil
}