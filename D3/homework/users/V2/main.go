package main

import (
	"fmt"
	"strings"
	"strconv"
)
//声明一个切片变量
var users=[]map[string]string{}
//获取用户ID的函数getID
func getId() string{
	id:=0
	//获取最大值
	for _,user:=range users{
		if uid,err:=strconv.Atoi(user["id"]);err==nil {
			if uid > id {
				uid=id
			}
		}
	}
	//最新ID
	return strconv.Itoa(id+1)
}
//输入函数input
func input(prompt string) string {
	var text string
	fmt.Print(prompt)
	fmt.Scan(&text)
	return text
}

//增加用户函数addUser
func addUser() {
	//声明一个映射变量
	user:=map[string]string {
		//使用短声明
		"id":getId(),
		"name":input("请输入用户名："),
		"addr":input("请输入地址："),
		"tel":input("请输入电话："),
	}
	users=append(users, user)
}


// func addUser() {
// 	fmt.Println("addUser")
// }
// func modifyUser() {
// 	fmt.Println("modifyUser")
// }
// func delUser() {
// 	fmt.Println("delUser")
// }
// func queryUser() {
// 	fmt.Println("queryUser")
// }


//按ID查找用户
func findUserById(id string) map[string]string {
	for _,user:=range users {
		if user["id"]==id {
			return user
		}
	}
	return nil
}

//按ID修改用户
func modifyUserById(user map[string]string,id string) {
	//用tuser,否则被覆盖
	for i,tuser:=range users {
		if tuser["id"]==id {
			users[i] = user
			break
		}
	}
}

//修改用户函数
func modifyUser() {
	id:=input("请输入要修改用户的ID：")
	user:=findUserById(id)
	if user==nil {
		fmt.Println("用户不存在")
	} else {
		fmt.Println("你要修改的用户信息如下：")
		fmt.Println(user)
		confirm:=input("确认修改吗？Y/N ")
		if strings.ToLower(confirm)=="y" || strings.ToLower(confirm)=="yes" {
			user:=map[string]string {
				"id":id,
				"name":input("请输入用户名："),
				"addr":input("请输入地址："),
				"tel":input("请输入电话："),
			}
			modifyUserById(user,id)
		}
	}
}

//过滤,是否包含filter
func filter(user map[string]string,q string) bool {
	return strings.Contains(user["name"],q) ||
		strings.Contains(user["addr"],q) || 
		strings.Contains(user["tel"], q) ||
		strings.Contains(user["id"],q)
}

//打印用户信息printUser
func printUser(user map[string]string){
	fmt.Println(user)
	//fmt.Println(user["id"])
}

//查询用户函数queryUser
func queryUser() {
	query:=input("请输入查询信息：")	
	fmt.Println("查询结果如下:")
	for _,user := range users {		
		if filter(user,query) ==true {			
			printUser(user)		
		} else {
			fmt.Println("无数据。")
			break
		}
	}

}


func delUserById(id string) {
	for i,user:=range users {
		if user["id"]==id {
			users = append(users[:i], users[i+1:]...)
		}
	}

}
//删除用户函数delUser
func delUser() {
	id:=input("请输入要删除用户的ID：")
	user:=findUserById(id)
	if user==nil {
		fmt.Println("用户不存在")
	} else {
		fmt.Println("你要删除的用户信息如下：")
		fmt.Println(user)
		confirm:=input("确认删除吗？Y/N ")
		if strings.ToLower(confirm)=="y" || strings.ToLower(confirm)=="yes" {
			delUserById(id)
		}
	}
}

func main() {
	//key:value结构，通过map替换
	operates:=map[string]func() {
		"add":addUser,
		"modify":modifyUser,
		"del":delUser,
		"query":queryUser,
	}
	for {
		var text string
		fmt.Printf("请输入指令：")
		fmt.Scan(&text)
		if text=="exit" {
			fmt.Println("退出")
			break
		}
		//op,ok map的kv值
		if op,ok:=operates[text];ok{
			op()
		} else {
			fmt.Println("指令错误")
		}
	}
}