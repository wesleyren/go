/*
 * @,@Author: ,: your name
 * @,@Date: ,: 2021-03-09 08:28:55
 * @,@LastEditTime: ,: 2021-03-09 16:34:12
 * @,@LastEditors: ,: Please set LastEditors
 * @,@Description: ,: In User Settings Edit
 * @,@FilePath: ,: \go\D3\home\funcs\modifyUser.go
 */
package funcs

import (
	"fmt"
	"strconv"
	"strings"

	define "gitee.com/wesleyren/go/tree/master/D5/homework/cmd/define"
	utils "gitee.com/wesleyren/go/tree/master/D5/homework/cmd/utils"
)

 func ModifyUser() {
	 var name,input string
	 fmt.Println("请输入要修改的信息")
	 input=utils.Read()
	 if s,err:=strconv.Atoi(strings.TrimSpace(input));err==nil {
		 id:=int64(s)
		 //fmt.Printf("idType: %T  idValue: %v\n", id, id)
		 user,err:=IdFindUser(&define.UserList,id)
		 if err!=nil {
			fmt.Println("未找到此用户.", user)
		 }else {
			 ShowUser(id)
			fmt.Printf("找到用户%v\nA确认修改%v?(y/n) ", user.Name, user.Name)
			input = utils.Read()
			if strings.ToLower(input) == "y" {
				fmt.Println("修改中")
				user,err:=IdModUser(&define.UserList,id)
				if err!=nil {
					fmt.Println(err)
				} else {
					fmt.Printf("修改用户:%v\n",user.Name)
					ShowUser(id)
				}
			} else if strings.ToLower(input) == "n" {
				fmt.Println("未修改。")
			}
		}
	 }else {
		name = strings.ToLower(strings.TrimSpace(input))
		//fmt.Printf("nameType: %T  nameValue: %v\n", name, name)
		user,err := NameFindUser(&define.UserList,name)
		if err!=nil {
			fmt.Println("无此用户",user)
		} else {
			ShowUser(user.Id)
			fmt.Printf("找到用户%v -->确认修改%v?(y/n)\n ", user.Name, user.Name)
			input = utils.Read()
			if strings.ToLower(input) == "y" {
				fmt.Println("修改中")
				NameModUser(&define.UserList, name)
			} else if strings.ToLower(input) == "n" {
				fmt.Println("未修改。")
			}
		}
 }
}