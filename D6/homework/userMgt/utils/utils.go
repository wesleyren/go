package utils

import (
	"bufio"
	"crypto/md5"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"time"

	"gitee.com/wesleyren/go/tree/master/D6/homework/userMgt/define"
)

//DateCheck make sure the input date is formatted
func DateCheck(d string) error {
	_,err:=time.Parse("2006.01.02",d)
	return err
}

//GenID gen a id by unix who's type is int64
func GenID()(res int64) {
	result := time.Now().UnixNano()
	return result
}

//JustDigits to verify if a string contain only digits 
func JustDigits(s string) bool {
	var a bool=true
	for _,c:=range s {
		if c<'0' || c>'9' {
			a=false
			break
		}
	}
	return a
}

//Read read content from standard input
func Read() string {
	scanner:=bufio.NewScanner(os.Stdin)
	scanner.Scan()
	line:=strings.TrimSpace(scanner.Text())
	return line
}

//GetField prom input the user's field
func GetField(f string) string {
	for _,field:=range define.UserField{
<<<<<<< HEAD
<<<<<<< HEAD
		if field==f {
=======
<<<<<<< HEAD
=======
>>>>>>> fab14c7366fe5da015364ccd20aad0774240f140
		if field==f {
=======
		if field=f {
>>>>>>> 57461d879041bf6b96580b7dff0837b0dc3346e5
<<<<<<< HEAD
>>>>>>> fab14c7366fe5da015364ccd20aad0774240f140
=======
>>>>>>> fab14c7366fe5da015364ccd20aad0774240f140
			fmt.Printf("Please input %v:",f)
			input:=Read()
			return input
		}
	}
	return f
}

//Genpwd gen [16]uint8 pwd
func GenPasswd()[16]uint8 {
	d:=[]byte(Read())
	return md5.Sum(d)
}

//GetKeyByValue return the same value's different keys
func GetKeyByValue(m  map[string]string,value string) []string {
	var keys []string
	for k,v:=range m {
		if v==value {
			keys=append(keys,k)
		}
	}
	return keys
}

//ArrayToString convert a string array to string
func ArrayToString(s []string) string {
	//b is a type of []uint8
	b,err:=json.Marshal(s)
	if err!=nil {
		log.Fatal(err)
	}
	return string(b)
}

//clear the console
//create a map for storing clear funcs
var clear map[string]func()
func init() {
	//init
	clear=make(map[string]func())
	clear["linux"]=func() {
		cmd:=exec.Command("clear")
<<<<<<< HEAD
<<<<<<< HEAD
		cmd.Stdout=os.Stdout
=======
<<<<<<< HEAD
=======
>>>>>>> fab14c7366fe5da015364ccd20aad0774240f140
		cmd.Stdout=os.Stdout
=======
		cmd.Stdout:=os.Stdout
>>>>>>> 57461d879041bf6b96580b7dff0837b0dc3346e5
<<<<<<< HEAD
>>>>>>> fab14c7366fe5da015364ccd20aad0774240f140
=======
>>>>>>> fab14c7366fe5da015364ccd20aad0774240f140
		cmd.Run()
	}
	clear["windows"]=func() {
		cmd:=exec.Command("cmd","/c","cls")
		cmd.Stdout=os.Stdout
		cmd.Run()
	}
}

//ClearScreen swip the chars on terminal
func ClearScreen() {
	//runtime.GOOS linux,windows,etc
	value,ok:=clear[runtime.GOOS]
	if ok {
		value()
	} else {
		panic("unsupported")
	}
}

//message print debug info
func Message(m string) {
	fmt.Println(m)
}

//quit cause program exit
func Quit() {
	os.Exit(define.UserQuit)
}