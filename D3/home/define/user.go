/*
 * @,@Author: ,: your name
 * @,@Date: ,: 2021-03-09 08:26:39
 * @,@LastEditTime: ,: 2021-03-09 12:47:18
 * @,@LastEditors: ,: Please set LastEditors
 * @,@Description: ,: In User Settings Edit
 * @,@FilePath: ,: \go\D3\home\define\user.go
 */
package define

var Id int64
//定义用户的结构体
type User struct {
	Name string
	Addr string
	Phone string
}

//定义用户列表，包含所有用户
var UserList []map[int64] User