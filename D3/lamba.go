package main

import (
	"fmt"
	"strings"
)

func print(formatter func(string) string,args ...string) {
	for i,v:=range args {
		fmt.Println(i,formatter(v))
	}
}

func add(a,b int) int {
	return a+b
}

func main() {
	// c:=func() {
	// 	fmt.Println("11")
	// }
	// fmt.Printf("%T\n",c)
	// c()
	names:=[]string{"aa","bb"}
	star:=func(txt string) string {
		return "*" + txt + "*"
	}
	print(star,names...)
	a,b:=1,2
	fmt.Println(add(a,b))
	fmt.Println(add(1,2))
	print(func(txt string) string {
		return "|"+txt+"|"
	},names...)
	fmt.Println(strings.FieldsFunc("AbaabcAbcabc", func(ch rune) bool {
		return ch == 'a'
	}))

	func() {
		fmt.Println("啦啦啦啦啦")
	}()
}