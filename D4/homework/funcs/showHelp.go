package funcs

import (
	"os"

	"github.com/olekukonko/tablewriter"
)

// func Default() {
// 	fmt.Print("\n|Illegal input|\ntype \"h\" show help list.\n> ")
// }
func ShowHelp() {
	t:=tablewriter.NewWriter(os.Stdout)
	t.SetAutoFormatHeaders(false)
	t.SetAutoWrapText(false)
	t.SetReflowDuringAutoWrap(false)

	t.SetHeader([]string{"CMD","Fuction"})
	t.Append([]string{"add","Add a user"})
	t.Append([]string{"del","Del a user"})
	t.Append([]string{"mod", "Modify a User"})
	t.Append([]string{"query", "Search User"})
	t.Append([]string{"show", "Show User List"})
	t.Append([]string{"cls", "Clean the terminal"})
	t.Append([]string{"help", "Show this help list"})
	t.Append([]string{"Q|q", "Exit"})
	t.Render()
}