package main

import (
	"fmt"
	"os"
	"time"
)

// func Open(name string) (*file,error) {
// 	return os.OpenFile(name,os.O_RDONLY,0777)
// }
// func Create(name string) (*file,error) {

// 	return os.OpenFile(name, os.O_WRONLY|os.O_CREATE|os.O_TRUNC)
// }
func main() {
	file,err:=os.OpenFile("test.log",os.O_WRONLY|os.O_CREATE|os.O_APPEND,0600)
	if err!=nil {
		return
	}
	defer file.Close()
	fmt.Fprintf(file,"%s\n",time.Now().Format("2011-01-02 15:00:22"))
	fmt.Fprintf(file, "%s\n", time.Now().Format("2005-01-02 15:04:05"))
}