/*
 * @,@Author: ,: your name
 * @,@Date: ,: 2021-03-09 08:28:55
 * @,@LastEditTime: ,: 2021-03-09 16:34:12
 * @,@LastEditors: ,: Please set LastEditors
 * @,@Description: ,: In User Settings Edit
 * @,@FilePath: ,: \go\D3\home\funcs\modifyUser.go
 */
 package funcs

 import (
	 "fmt"
	 "strconv"
	 "strings"
	define "gitee.com/wesleyren/go/tree/meastr/D3/home/define"
	utils "gitee.com/wesleyren/go/tree/meastr/D3/home/utils"
)

 func ModifyUser() {
	 var name,input string
	 fmt.Println("请输入要修改的信息")
	 input=utils.Read()
	 if s,err:=strconv.Atoi(strings.TrimSpace(input));err==nil {
		 id:=int64(s)
		 //fmt.Printf("idType: %T  idValue: %v\n", id, id)
		 user:=utils.QueryUserById(define.UserList,id)
		 if user.Name == "" {
			fmt.Println("未找到此用户.", user)
		} else {
			fmt.Printf("找到用户%v --> %v\nA确认修改%v?(y/n) ", name, user, name)
			input = utils.Read()
			if strings.ToLower(input) == "y" {
				fmt.Println("修改中")
				utils.ModifyUserById(&define.UserList,id)
			} else if strings.ToLower(input) == "n" {
				fmt.Println("未修改。")
			}
		}
	 }else {
		name = strings.ToLower(strings.TrimSpace(input))
		//fmt.Printf("nameType: %T  nameValue: %v\n", name, name)
		user := utils.QueryUserByName(name)
		if user == nil {
			fmt.Println("未找到此用户.", user)
		} else {
			fmt.Printf("找到用户%v -->确认修改%v?(y/n)\n ", name, user)
			input = utils.Read()
			if strings.ToLower(input) == "y" {
				fmt.Println("修改中")
				utils.ModifyUserByName(&define.UserList, name)
			} else if strings.ToLower(input) == "n" {
				fmt.Println("未修改。")
			}
		}
	 }
 }