/*
 * @,@Author: ,: your name
 * @,@Date: ,: 2021-03-09 08:28:48
 * @,@LastEditTime: ,: 2021-03-09 16:07:10
 * @,@LastEditors: ,: Please set LastEditors
 * @,@Description: ,: In User Settings Edit
 * @,@FilePath: ,: \go\D3\home\funcs\delUser.go
 */
package funcs

import (
	"fmt"
	"strconv"
	"strings"

	define "gitee.com/wesleyren/go/tree/master/D5/homework/cmd/define"
	utils "gitee.com/wesleyren/go/tree/master/D5/homework/cmd/utils"
)

func DelUser() {
	 var name,input string
	 utils.Message("输入你想删除的用户的ID/name")
	 input=utils.GetField("Name")
	 if i,err:=strconv.Atoi(strings.TrimSpace(input));err==nil {
		 id:=int64(i)
		 user,_:=IdFindUser(&define.UserList,id)
		 if user.Name=="" {
			 fmt.Println("用户不存在")
		 } else {
			 ShowUser(id)
			 fmt.Printf("找到%v,确认要删除么%v?(y/n)\n",user.Name,user.Name)
			 input=utils.Read()
			 if strings.ToLower(input)=="y" {
				if err:=IdDelUser(&define.UserList,id);err!=nil {
					fmt.Println(err)
				} else {
					fmt.Println("删除")
				}
			}else if strings.ToLower(input)=="n" {
				 fmt.Println("未操作")
			 }else {
				 fmt.Println("输入错误")
			 }
		 }
	 } else {
		 name=strings.ToLower(strings.TrimSpace(input))
		user,_:=NameFindUser(&define.UserList,name)
		 if (user==define.User{}) {
			 fmt.Println("用户不存在")
		 } else {
			 //ShowUser(user.Id)
			fmt.Printf("找到%v,确认要删除么%v?(y/n)\n",user.Name,user.Name)
			input=utils.Read()
			if strings.ToLower(input)=="y" {
				if err:=NameDelUser(&define.UserList,name);err!=nil {
					fmt.Println(err)
				} else {
					fmt.Println("删除")
				}
			}else if strings.ToLower(input)=="n" {
				fmt.Println("未操作")
			}else {
				fmt.Println("输入错误")
			}
		 }
	 }
}