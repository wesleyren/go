package funcs

import (
	"crypto/md5"
	"fmt"

	define "gitee.com/wesleyren/go/tree/master/D5/homework/cmd/define"
	utils "gitee.com/wesleyren/go/tree/master/D5/homework/cmd/utils"
)

var p string="admin"
var password [16]byte=md5.Sum([]byte(p))

func Login() bool {
	var logCount int = 0
	var logged bool = true
	for {
		fmt.Println("请输入密码")
		input:=utils.Read()
		user,err:=NameFindUser(&define.UserList,input)
		if err!=nil {
			fmt.Println("用户不存在")
			logCount++
			if logCount==3 {
				logged=false
				break
			}
		} else {
			fmt.Println("输入正确的密码")
			input:=utils.Read()
			inputPasswd:=md5.Sum([]byte(input))
			if user.Passwd==inputPasswd {
				fmt.Printf("用户%s登录成功\n",user.Name)
				return logged
			} else {
				fmt.Println("密码错误")
			}
			logCount++
			if logCount==3 {
				logged=false
				break
			}
			continue
		}
	}
	return logged
}