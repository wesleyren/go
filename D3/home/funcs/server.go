/*
 * @,@Author: ,: your name
 * @,@Date: ,: 2021-03-09 11:26:57
 * @,@LastEditTime: ,: 2021-03-09 12:58:13
 * @,@LastEditors: ,: Please set LastEditors
 * @,@Description: ,: In User Settings Edit
 * @,@FilePath: ,: \go\D3\home\funcs\server.go
 */
package funcs

 import (
	"fmt" 
	define "gitee.com/wesleyren/go/tree/meastr/D3/home/define"
	utils "gitee.com/wesleyren/go/tree/meastr/D3/home/utils"
)
 
 func Serv() {
	 var op string
	 help := func() {
		 fmt.Print("1. addUser\n2. delUser\n3. modifyUser\n" +
			 "4. queryUser\n5. showUserList\nc. clearConsole\nh. showHelp\nq. Quit\n\n> ")
	 }
	 help()
	 for {
		 fmt.Scanln(&op)
		 switch op {
		 case "1":
			 fmt.Print("\n|addUser|\n")
			 AddCurrentUser()
			 op = ""
			 continue
		 case "2":
			 fmt.Printf("\n|delUser|\n")
			 DelUser()
			 op = ""
			 continue
		 case "3":
			 fmt.Printf("\n|modUser|\n")
			 ModifyUser()
			 op = ""
			 continue
		 case "4":
			 fmt.Printf("\n|queryUser|\n")
			 SearchUser(&define.UserList)
			 op = ""
			 continue
		 case "5":
			 fmt.Printf("\n|showUserList|\n")
			 ShowUserList()
			 op = ""
			 continue
		 case "h":
			 fmt.Printf("\n|showHelp|\n")
			 help()
			 op = ""
			 continue
		 case "c":
			 fmt.Printf("\n|clearConsole|\n")
			 utils.CallClear()
			 fmt.Print("[\"h\" for help]> ")
			 op = ""
			 continue
		 case "":
			 fmt.Print("[\"h\" for help]> ")
		 case "q", "Q":
			 return
		 default:
			 fmt.Print("\n|Illegal input|\ntype \"h\" show help list.\n> ")
			 op = ""
			 continue
		 }
	 }
 }
 