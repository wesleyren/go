package manager

import (
	"fmt"

	"gitee.com/wesleyren/go/D3/users/v4/utils"
)

//路由
var routers=map[string] func() {}
//注册机制
func Register(op string,callback func()) {
	if _,ok:=routers[op];ok {
		//panic 疑问点
		panic(fmt.Sprintf("指令%s已存在",op))
	}
	routers[op]=callback
}

func Run() {
	for {
		text := utils.Input("请输入指令: ")
		if text == "exit" {
			fmt.Println("退出")
			break
		}
		if action, ok := routers[text]; ok {
			action()
		} else {
			fmt.Println("指令错误")
		}
	}
}

	